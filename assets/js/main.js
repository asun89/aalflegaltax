(function ($) {
 "use strict";
/*----------------------------

 jQuery MeanMenu

------------------------------ */

	jQuery('nav#dropdown').meanmenu();		

/*----------------------------

 wow js active

------------------------------ */

 new WOW().init();

	$('.scrollup').on("click",function() {

	    $('html, body').animate({ scrollTop: 0 }, 'slow');

	}); 

	var counter_start = false;

  function isMobilePage(){
    if($(window).width() < 576){
      return true;
    }
    return false;
  }

	$(window).scroll( function(){
    if(isMobilePage()){
      $("#welcome-to-aalf-legal-tax-home-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          
          if( bottom_of_window > (bottom_of_object) ){
              $(this).animate({"margin-left":'0px', opacity:1},500);   
          }
      });
    } else {
      $("#welcome-to-aalf-legal-tax-home").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-left":'0px', opacity:1},500);   
        }
    });
    }

    //Scrolling Animation Banner
    if(!isMobilePage()){
      $("#fadein-home-banner").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'40%', opacity:1},500);   
        }
    });

      $("#about-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#contact-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#client-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#career-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#team-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#partner-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#expertise-content").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
    }
    else {

      //Mobile
      $("#fadein-home-banner-mobile").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'40%', opacity:1},500);   
        }
    });
      $("#about-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#contact-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#client-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#career-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
      $("#team-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#partner-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });

      $("#expertise-content-mobile").each(function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);   
        }
      });
    }

    //Scrolling Animation Banner

    $(".top-about").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if( bottom_of_window > bottom_of_object ){
          $(this).animate({"margin-top":'0px', opacity:1},500);   
      }
    });

    $(".bottom-about").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      if( bottom_of_window > bottom_of_object ){
          $(this).animate({"margin-top":'0px', opacity:1},500);   
      }
    });

  

    $("#home-content-1").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){ 
            $(this).animate({"margin-top":'0px', opacity:1},500);
        }
    });

    $("#home-content-2").each(function(i){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
            $(this).animate({"margin-top":'0px', opacity:1},500);
        }
    });

		$('.hide_it_first').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object / 1.5) ){
            
            $(this).animate({'opacity':'1'},500);
                
        }
        
    }); 

    $('.years_of_experience').each(function(i){
    	var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > (bottom_of_object) ){
        	if(counter_start == false){
           	setTimeout(function countingUp() {
				var angka = parseInt($(".exp-counter").html());
				if(angka < 22){
					angka = angka + 1;
					if(angka < 10){
						$(".exp-counter").html("0" + angka);
					} else {
						$(".exp-counter").html(angka);
					}
				}

			    setTimeout(countingUp, 300);
			}, 300);     
         	}
        }
     });
	});

  $("#submit-message").mouseout(function(){
    $(this).css("background", "#C6AC71");
    $(this).css("color", "#FFFFFF");
  });

  $("#submit-message").mouseover(function(){
    $(this).css("background", "#FFFFFF");
    $(this).css("color", "#C6AC71");
  });

  $(".get-a-consultant-btn").mouseout(function(){
    $(this).css("background", "#C6AC71");
    $(this).css("color", "#FFFFFF");
  });

  $(".get-a-consultant-btn").mouseover(function(){
    $(this).css("background", "#FFFFFF");
    $(this).css("color", "#C6AC71");
  });

  var submitting = 0;

  $("#contact_form").submit(function(){
    if(submitting == 1){
      return false;
    }

    var contact_form = $(this);
    contact_form.find("input").each(function(){
      $(this).attr("readonly", true);
      $(this).css("background-color","#CCCCCC");
    });
    contact_form.find("button").each(function(){
      $(this).attr("readonly", true);
      $(this).attr("disabled", true);
      $(this).css("background-color","#CCCCCC");
    });
    contact_form.find("textarea").each(function(){
      $(this).attr("readonly", true);
      $(this).css("background-color","#CCCCCC");
    });
    $("#submit-message").val("SUBMITTING...");
    $("#submit-message").css("readonly", true);
    $("#submit-message").css("disabled", true);
    submitting = 1;

    $.ajax({
       type: "POST",
       url: contact_form.attr("action"),
       data: contact_form.serialize(), // serializes the form's elements.
       success: function(data)
       {
        if(data == "success"){
           Swal.fire({
            title: 'Submitted Successfully!',
            text: 'Thanks for your inquiry, we will contact you back soon',
            icon: 'success',
            confirmButtonText: 'Ok',
            width: "30%"
          }).then(function(isConfirm){
            window.location = "";
          });

           contact_form.find("input").each(function(){
            $(this).val("");
          });

           contact_form.find("textarea").each(function(){
            $(this).val('');
          });

         } else {
          Swal.fire({
            title: 'Submit Failed!',
            text: 'There are some errors in your input, please check again and try to input again',
            icon: 'error',
            confirmButtonText: 'Ok',
            width: "30%"
          }).then(function(isConfirm){
            window.location = "";
          });
         }

          contact_form.find("input").each(function(){
            $(this).attr("readonly", false);
            $(this).css("background-color","#FFFFFF");
          });
          contact_form.find("button").each(function(){
            $(this).attr("readonly", false);
            $(this).attr("disabled", false);
      $(this).css("background-color","#FFFFFF");
          });
          contact_form.find("textarea").each(function(){
            $(this).attr("readonly", false);
      $(this).css("background-color","#FFFFFF");
          });

          $(".get-a-consultant-btn").css("background", "#C6AC71");
          $(".get-a-consultant-btn").css("color", "#FFFFFF");

          $("#submit-message").css("background", "#C6AC71");
          $("#submit-message").css("color", "#FFFFFF");
          $("#submit-message").val("SEND MESSAGE");
          $("#submit-message").css("readonly", false);
          $("#submit-message").css("disabled", false);
          submitting = 0;
       }
     });
    return false;
  });

  $(".scrollup").mouseover(function(){
    var alternate_src = $(this).find("img").attr("alternate-src");
    var src = $(this).find("img").attr("src");

    var alternate_src_arrow = $(this).find("img").attr("alternate-src");
    var src_arrow = $(this).find("img").attr("src");

    $(this).find("img").attr("src", alternate_src);
    $(this).find("img").attr("alternate-src", src);

    $(this).find("img").attr("src", alternate_src_arrow);
    $(this).find("img").attr("alternate-src", src_arrow);
  });

  $(".scrollup").mouseout(function(){
    var alternate_src = $(this).find("img").attr("alternate-src");
    var src = $(this).find("img").attr("src");


    var alternate_src_arrow = $(this).find("img").attr("alternate-src");
    var src_arrow = $(this).find("img").attr("src");

    $(this).find("img").attr("src", alternate_src);
    $(this).find("img").attr("alternate-src", src);

    $(this).find("img").attr("src", alternate_src_arrow);
    $(this).find("img").attr("alternate-src", src_arrow);
  });

  $(".link-arrow-footer").mouseover(function(){
    var alternate_src = $(this).find("img").attr("alternate-src");
    var src = $(this).find("img").attr("src");

    var alternate_src_arrow = $(this).find("img").attr("alternate-src");
    var src_arrow = $(this).find("img").attr("src");

    $(this).find("img").attr("src", alternate_src);
    $(this).find("img").attr("alternate-src", src);

    $(this).find("img").attr("src", alternate_src_arrow);
    $(this).find("img").attr("alternate-src", src_arrow);
  });

  $(".link-arrow-footer").mouseout(function(){
    var alternate_src = $(this).find("img").attr("alternate-src");
    var src = $(this).find("img").attr("src");


    var alternate_src_arrow = $(this).find("img").attr("alternate-src");
    var src_arrow = $(this).find("img").attr("src");

    $(this).find("img").attr("src", alternate_src);
    $(this).find("img").attr("alternate-src", src);

    $(this).find("img").attr("src", alternate_src_arrow);
    $(this).find("img").attr("alternate-src", src_arrow);
  });

	$(".expertise-icon").mouseover(function(){
    if(!isMobilePage()){
      var alternate_src = $(this).find(".expertise-img").attr("alternate-src");
      var src = $(this).find(".expertise-img").attr("src");


      var alternate_src_arrow = $(this).find(".arrow-img-home").attr("alternate-src");
      var src_arrow = $(this).find(".arrow-img-home").attr("src");

      $(this).find(".expertise-img").attr("src", alternate_src);
      $(this).find(".expertise-img").attr("alternate-src", src);

      $(this).find(".arrow-img-home").attr("src", alternate_src_arrow);
      $(this).find(".arrow-img-home").attr("alternate-src", src_arrow);
    }
	});

	$(".expertise-icon").mouseout(function(){
    if(!isMobilePage()){
  		var alternate_src = $(this).find(".expertise-img").attr("alternate-src");
  		var src = $(this).find(".expertise-img").attr("src");


      var alternate_src_arrow = $(this).find(".arrow-img-home").attr("alternate-src");
      var src_arrow = $(this).find(".arrow-img-home").attr("src");

  		$(this).find(".expertise-img").attr("src", alternate_src);
  		$(this).find(".expertise-img").attr("alternate-src", src);

      $(this).find(".arrow-img-home").attr("src", alternate_src_arrow);
      $(this).find(".arrow-img-home").attr("alternate-src", src_arrow);
    }
	});

  $(".partner-name-link").mouseover(function(){
    if($(this).attr("clicked") == "false"){
      var alternate_src = $(this).find(".partner-link-border-img").attr("alternate-src");
      var src = $(this).find(".partner-link-border-img").attr("src");

      $(this).find(".partner-link-border-img").attr("src", alternate_src);
      $(this).find(".partner-link-border-img").attr("alternate-src", src);
    }
  });

  animateClickedPartnerName($("#initial-partner"));
  clickActionPartner($("#initial-partner"));

  animateClickedPartnerNameMobile($("#initial-partner-mobile"));
  clickActionPartnerMobile($("#initial-partner-mobile"));

  $(".partner-name-link").mouseout(function(){
    if($(this).attr("clicked") == "false"){
      var alternate_src = $(this).find(".partner-link-border-img").attr("alternate-src");
      var src = $(this).find(".partner-link-border-img").attr("src");

      $(this).find(".partner-link-border-img").attr("src", alternate_src);
      $(this).find(".partner-link-border-img").attr("alternate-src", src);
    }
  });


  $(".partner-name-link").click(function(){
    var partner_name_link = $(this);
    $(".partner-name").each(function(){
      $(this).css("color", "#808080");
    });


    animateClickedPartnerName(partner_name_link);
    animateClickedPartnerNameMobile(partner_name_link);

    function clickActionPartnerThis(){
      clickActionPartner(partner_name_link);
      clickActionPartnerMobile(partner_name_link);
    }

    $(".partner_need_to_hide").animate({top:'100px', opacity:0}).delay("fast").animate({top:'0px', opacity:1});
    setTimeout(clickActionPartnerThis, 500)

    return false;
  });

  $(".expertise-toggle").click(function(){
    var show_target = $(this).attr("show-target");
    var state = $(this).attr("state");
    var plus_icon = $(this).find("#aoe-page-right-title").html();

    if(state == "0"){
      $("#" + show_target).slideDown( "fast");
      $(this).attr("state", "1");
    } else {
      $("#" + show_target).slideUp( "fast");
      $(this).attr("state", "0");
    } 
    
    if(plus_icon == "+"){
      $(this).find("#aoe-page-right-title").html("-");
    } else {
      $(this).find("#aoe-page-right-title").html("+");
    }

    return false;
  });

  $(".career-list").click(function(){
    var width = "25px";
    var height = "13px";
    var target = $(this).attr("target");
    var state = $(this).attr("state");
    var src = $(this).find(".arrow").attr("src");
    var alternate = $(this).find(".arrow").attr("alternate-src");

    if(isMobilePage()){
      width = "10px";
      height = "5px";
    }

    if(state == "closed"){
      $(this).attr("state", "opened");
      $("#"+target).slideDown();
      $(this).find(".career-job-title").css("color", "#C6AC71");
      $(this).find(".career-job-title").css("color", "#C6AC71");

      $(this).find(".arrow").css("width", width);
      $(this).find(".arrow").css("height", height);
    } else {
      $(this).attr("state", "closed");
      $("#"+target).slideUp();
      $(this).find(".career-job-title").css("color", "#6D6E6B");
      $(this).find(".career-job-title").css("color", "#6D6E6B");
      $(this).find(".arrow").css("height", width);
      $(this).find(".arrow").css("width", height);
    }
    $(this).find(".arrow").attr("src", alternate);
    $(this).find(".arrow").attr("alternate-src", src);
  });

  $(".lightSlider").lightSlider({
item:1

  });

  $(".lightSlider3").lightSlider({
item:3

  });
  
})(jQuery); 

function animateClickedPartnerName(partner_name_link){
    var src = "assets/img/border-under.svg";
    var alternate_src = "";

    $(".partner-name-link").each(function(){
      $(this).attr("clicked", "false");
    });
    $(".border-under-title-left-tiny").each(function(){
      var alternate_src = "assets/img/border-under.svg";
      var src = "";

      $(this).find(".partner-link-border-img").attr("src", src);
      $(this).find(".partner-link-border-img").attr("alternate-src", alternate_src);
      $(this).css("width", "100px");
      $(this).css("border-top-height", "1px");
      $(this).css("border-top-style", "solid");
      $(this).css("border-top-color", "#999999");
    });
    partner_name_link.find(".border-under-title-left-tiny").css("width", "100%");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-height", "1px");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-style", "solid");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-color", "#C6AC71");
    partner_name_link.find(".partner-name").css("color", "#C6AC71");
    partner_name_link.find(".partner-link-border-img").attr("src", src);
    partner_name_link.find(".partner-link-border-img").attr("alternate-src", alternate_src);
    partner_name_link.attr("clicked", "true");
}

function animateClickedPartnerNameMobile(partner_name_link){
    var src = "assets/img/border-under.svg";
    var alternate_src = "";

    $(".partner-name-link").each(function(){
      $(this).attr("clicked", "false");
      $(this).find(".partner-name-mobile").css("color", "#808080");
      $(this).find(".partner-name-mobile").css("background-color", "#FFFFFF");
    });
    $(".border-under-title-left-tiny").each(function(){
      var alternate_src = "assets/img/border-under.svg";
      var src = "";

      $(this).find(".partner-link-border-img").attr("src", src);
      $(this).find(".partner-link-border-img").attr("alternate-src", alternate_src);
      $(this).css("width", "100px");
      $(this).css("border-top-height", "1px");
      $(this).css("border-top-style", "solid");
      $(this).css("border-top-color", "#999999");
    });
    partner_name_link.find(".border-under-title-left-tiny").css("width", "100%");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-height", "1px");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-style", "solid");
    partner_name_link.find(".border-under-title-left-tiny").css("border-top-color", "#C6AC71");
    partner_name_link.find(".partner-name-mobile").css("color", "#FFFFFF");
    partner_name_link.find(".partner-name-mobile").css("background-color", "#C6AC71");
    partner_name_link.find(".partner-link-border-img").attr("src", src);
    partner_name_link.find(".partner-link-border-img").attr("alternate-src", alternate_src);
    partner_name_link.attr("clicked", "true");
}

function clickActionPartner(clickTarget){

  var fullname = clickTarget.attr("fullname");
  var title = clickTarget.attr("title");
  var description_1 = clickTarget.attr("description_1");
  var description_2 = clickTarget.attr("description_2");
  var photo = clickTarget.attr("photo");
  var education = clickTarget.attr("education");

  $("#partner_fullname").html(fullname);
  $("#partner_title").html(title);
  $("#partner_description_1").html(description_1);
  $("#partner_description_2").html(description_2);
  $("#partner_photo").attr("src", photo);
  $("#partner_education").html(education);
}

function clickActionPartnerMobile(clickTarget){

  var fullname = clickTarget.attr("fullname");
  var title = clickTarget.attr("title");
  var description_1 = clickTarget.attr("description_1");
  var description_2 = clickTarget.attr("description_2");
  var photo = clickTarget.attr("photo");
  var education = clickTarget.attr("education");

  $("#partner_fullname_mobile").html(fullname);
  $("#partner_title_mobile").html(title);
  $("#partner_description_1_mobile").html(description_1);
  $("#partner_description_2_mobile").html(description_2);
  $("#partner_photo_mobile").attr("src", photo);
  $("#partner_education_mobile").html(education);
}

function loadingScreen(){
    if(getCookie("loaded") != "true"){
    	$("body").css("background-color", "#C6AC71");
    	var show = 1;
        setTimeout(function blink() {
            if(show == 1){
                $("#logo-intro").animate({'opacity':'0'},500);
                show = 0;
            } else {
                $("#logo-intro").animate({'opacity':'1'},500);
                show = 1;
            }
            setTimeout(blink, 500);
        }, 500);   

        var borderwidth = 0;
        var maxloadingbar = $("#maxloadingbar").width();
        setTimeout(function grow() {
        	if (document.getElementsByTagName('body')[0] !== undefined) {
        		$("#website-loading").animate({'opacity':'0'},500, function(){
        			$("body").css("background-color", "#FFFFFF");
        			$("#website-content").animate({'opacity':'1'},500);
        			$("#website-loading").hide();
        		});
        		
        	} 

            var width = $("#loadingbar").width();
            if((maxloadingbar - width) <= 20){
                width = 0;
            } else if(maxloadingbar >= width){
                width = width + 20;
            }
            $("#loadingbar").width(width);
            setTimeout(grow, 50);
        }, 50); 
        setCookie("loaded", "true", 1);
      } 
      else {
        $("#website-content").show();
        $("#website-loading").hide();
        $("#logo-intro").css('opacity','0');
        $("#website-content").css('opacity','1');
        $("#website-loading").css('opacity','1');
      }

      
  }

  function setCookie(name,value,days) {
      var expires = "";
      if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days*24*60*60*1000));
          expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
  function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
  }
