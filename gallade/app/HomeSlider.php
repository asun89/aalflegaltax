<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'home_slider';
}
