<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@Index');
Route::get('/home', 'HomeController@Index');
Route::get('/about', 'AboutController@Index');
Route::get('/expertises', 'ExpertisesController@Index');
Route::get('/partners', 'PartnersController@Index');
Route::get('/teams', 'TeamsController@Index');
Route::get('/clients', 'ClientsController@Index');
Route::get('/career', 'CareerController@Index');
Route::get('/article', 'ArticleController@Index');
Route::get('/article/open-article', 'ArticleController@Open');
Route::post('/career/submit', 'CareerController@Submit');
Route::get('/contact', 'ContactController@Index');
Route::post('/contact/submit', 'ContactController@Submit')->middleware('web');