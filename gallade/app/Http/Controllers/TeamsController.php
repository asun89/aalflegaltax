<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Teams;

use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        // $teams = Teams::where('language_code', '=', $request->session()->get('lang'))->orderBy('teams_precedence', 'ASC')->get();
        $teams = Teams::orderBy('id', 'ASC')->get();
        
        return view('teams', array('themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js,
            "teams" => $teams,
            "meta_title" => "Our Teams",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        ));
    }
}