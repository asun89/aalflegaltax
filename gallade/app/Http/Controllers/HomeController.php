<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

//Models
use App\HomeSlider;
use App\PracticeArea;
use App\Events;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        //\App::setLocale($locale);

        $this->themes = 'Home-three';
  //       $home_slider = HomeSlider::where('language_code', '=', $request->session()->get('lang'))->get();
  //       $practice_area = PracticeArea::where('language_code', '=', $request->session()->get('lang'))->get();

  //       $count_practice_area = count($practice_area);

  //       $a = 0;
		// $practice_area1 = array();
		// $practice_area2 = array();
  //       for($a = 0; $a < ($count_practice_area / 2); $a++)
  //       {
  //           $practice_area1[] = $practice_area[$a];
  //       }

  //       for($b = $a; $b < $a + ($count_practice_area / 2); $b++)
  //       {
  //           $practice_area2[] = $practice_area[$b];
  //       }
        
        // $events = Events::where(
        //     'language_code', '=', $request->session()->get('lang')
        // )->orderBy('id', 'DESC')->paginate(6);

        return view('home', 
        	array(
        		'themes' => $this->themes,
                'js_files' => $this->js_files,
                'css_files' => $this->css_files,
                'custom_js' => $this->custom_js, 
                'loaded' => isset($_COOKIE['loaded'])? $_COOKIE['loaded'] : 'false',
            "meta_title" => "Home",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        	)
        );
    }
}