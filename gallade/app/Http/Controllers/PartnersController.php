<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Partners;

use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $partners = Partners::where('language_code', '=', $request->session()->get('lang'))->get();
        
        return view('partners', array('themes' => $this->themes,
            'js_files' => $this->js_files,
            'css_files' => $this->css_files,
            'custom_js' => $this->custom_js,
            "partners" => $partners,
            "meta_title" => "Partners",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  ));
    }
}