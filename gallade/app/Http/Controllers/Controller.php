<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $js_files = array();
    protected $css_files = array();
    protected $custom_js = '';
    protected $themes = '';

    //We thread it as contstants 
    //protected $career_email = 'cybercreation89@gmail.com';
    protected $career_email = 'aalfcareer@gmail.com';
}
