<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Clients;

use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $clients = Clients::orderBy("id", "ASC")->get();

        return view('clients', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js, 
        	'clients' => $clients,
            "meta_title" => "Our Clients",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        ));
    }
}