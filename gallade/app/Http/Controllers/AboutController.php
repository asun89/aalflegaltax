<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\AboutUsContent;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(Request $request)
    {
 		$this->themes = 'Home-three';
 		// $about_content = AboutUsContent::where('language_code', '=', $request->session()->get('lang'))->get();
 		// $about = array();
 		// foreach($about_content as $ac)
 		// {
 		// 	$about[$ac->about_us_content_type] = $ac->about_us_content_text;
 		// }

        $about = array();

        return view('about', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js,
        	"about_content" => $about,
            "meta_title" => "About Us",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        	)
        );
    }
}