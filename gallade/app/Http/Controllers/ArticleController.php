<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Articles;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';
        $articles = Articles::orderBy("updated_datetime", "DESC")->get();

        return view('article', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js, 
        	'articles' => $articles,
            "meta_title" => "Article",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        ));
    }

    public function open(Request $request){
        $link = $request->input('link');
        $filename = 'assets/article_files/'.$link;
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($filename));
        header('Accept-Ranges: bytes');
        @readfile($filename);
    }
}