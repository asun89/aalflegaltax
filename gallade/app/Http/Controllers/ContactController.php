<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\ContactSubmission;
use App\ContactInquiry;
use App\Mail\ContactSendInquiry;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $this->themes = 'Home-three';

        $this->custom_js = "
        ";

        return view('contact', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js,
            "meta_title" => "Contact Us",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        	)

        );
    }

    public function submit(Request $request)
    {
        $post = $request->input();
        //Send Email and Insert to Database
        
        $validator = Validator::make($request->all(), [
           'name' => ['required','max:255','regex:/^[A-Za-z ]+$/'],
           'email' => ['required','max:255', 'regex:/^[A-Za-z0-9_.@ ]+$/'],
           'phone' => ['required', 'max:20', 'regex:/^[0-9]+$/'],
           'subject' => ['required', 'regex:/^[A-Za-z0-9_.,()\- ]+$/'],
           'message' => ['required', 'regex:/^[A-Za-z0-9_.,()\- ]+$/'],
       ]);

        if ($validator->fails()) {
            echo 'failed';
            exit;
        } else {
            $name = $post['name'];
            $email = $post['email'];
            $phone = $post['phone'];
            $subject = $post['subject'];
            $message = $post['message'];

            $contact_submission = new ContactSubmission;
            $contact_submission->name = $name;
            $contact_submission->email = $email;
            $contact_submission->phone = $phone;
            $contact_submission->subject = $subject;
            $contact_submission->message = $message;
            $contact_submission->created_at = date('Y-m-d H:i:s');
            $contact_submission->updated_at = date('Y-m-d H:i:s');
            $contact_submission->deleted_at = NULL;
            $contact_submission->save();

            $contact_inquiry = new ContactInquiry();
            $contact_inquiry->contact_name = $name;
            $contact_inquiry->contact_email = $email;
            $contact_inquiry->contact_phone = $phone;
            $contact_inquiry->contact_subject = $subject;
            $contact_inquiry->contact_message = $message;

            $data = array(
                'contact_inquiry' => $contact_inquiry
            );

            // Mail::send('emails.contact_send_inquiry', $data, function ($m) use ($contact_inquiry) {
            //     $m->from('cybercreation89@gmail.com', 'No-Reply aalflegaltax.com');
            //     $m->to($contact_inquiry->contact_email, $contact_inquiry->contact_name)->subject("Contact Submission at Ariyanto & Arnaldo Legal & Tax");
            // });


            echo 'success';
        }
    }
}
