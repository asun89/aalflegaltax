<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

use App\Expertise;

use Illuminate\Http\Request;

class ExpertisesController extends Controller
{
    public function index(Request $request)
    {
 		$this->themes = 'Home-three';
 		$expertises = Expertise::where('language_code', '=', $request->session()->get('lang'))->get();
 		// foreach($about_content as $ac)
 		// {
 			// $about[$ac->about_us_content_type] = $ac->about_us_content_text;
 		// }

        return view('expertises', array(
        	'themes' => $this->themes,
        	'js_files' => $this->js_files,
        	'css_files' => $this->css_files,
        	'custom_js' => $this->custom_js,
        	"expertises" => $expertises,
            "meta_title" => "Our Expertise",
            "meta_description" => "Ariyanto Arnaldo Legal & Tax Consultants, handling business law and legal & tax"  
        	)
        );
    }
}