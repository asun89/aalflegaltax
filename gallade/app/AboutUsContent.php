<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUsContent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'about_us_content';
}
