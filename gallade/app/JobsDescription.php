<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsDescription extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs_description';

    public function jobs()
    {
        return $this->belongsTo('App\Jobs');
    }
}
