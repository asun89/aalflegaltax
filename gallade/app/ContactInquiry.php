<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInquiry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    public $contact_name;
    public $contact_email;
    public $contact_phone;
    public $contact_subject;
    public $contact_message;
}
