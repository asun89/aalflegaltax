<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'expertise';
}
