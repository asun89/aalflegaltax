<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsApp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    public $applicant_name;
    public $applicant_position;
    public $applicant_email;
    public $applicant_phone;
    public $applicant_facebook;
    public $applicant_instagram;
    public $applicant_cv;
    public $applicant_to;
}
