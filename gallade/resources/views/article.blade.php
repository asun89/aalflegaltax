@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/client-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="client-content">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Experienced practitioners with<br/>multi disciplinary backgrounds</h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>ARTICLE</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/client-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="client-content-mobile">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Experienced practitioners with<br/>multi disciplinary backgrounds</h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>ARTICLE</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area">
 <div class="container">
   <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="section-title-area-article">
     <h2>{{trans('messages.article_announcement')}}</h2>
     </div>
     <div class="border-under-title margin-after-border desktop"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
   </div>
   </div>
</div>
</div>

<div class="article-container desktop">
	<div class="container">
		<ul class="lightSlider3">
		<?php foreach($articles as $article){ ?>
			<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12 article-subcontainer">
				<div class="article-thumbnail">
					<img src="{{URL::asset('assets/img/article_thumbnail/'.$article->thumbnail)}}"/>
				</div>
				<div class="article-date"><?php echo date("F j, Y", strtotime($article->updated_datetime)); ?></div>
				<div class="article-title"><?php echo substr($article->title, 0, 25)."..."; ?></div>
				<div class="article-page-border-under-title"><img src="{{URL::asset('assets/img/border-under-small-thick.svg')}}"></div>
		  		<div class="article-content"><?php echo substr($article->content, 0, 200); ?>.......</div>
				<div class="article-link"><a target="_blank" href="{{URL::asset('assets/article_files/'.$article->link)}}">Download full article</a></div>
			</li>
		<?php } ?>
		</ul>
	</div>
</div>

<div class="article-container mobile">
	<div class="container article-mobile-container">
		<ul class="lightSlider">
		<?php $count = 0; foreach($articles as $article){ ?>
			<?php if($count == 0) {?>
			<li class="col-lg-4 col-md-4 col-sm-12 col-xs-12 article-subcontainer">
			<?php } ?>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="article-thumbnail">
						<img src="{{URL::asset('assets/img/article_thumbnail/'.$article->thumbnail)}}"/>
					</div>
					<div class="article-date"><?php echo date("F j, Y", strtotime($article->updated_datetime)); ?></div>
					<div class="article-title"><?php echo $article->title; ?></div>
					<div class="article-page-border-under-title"><img src="{{URL::asset('assets/img/border-under-small-thick.svg')}}"></div>
			  		<div class="article-content"><?php echo $article->content; ?>.......</div>
					<div class="article-link"><a target="_blank" href="{{URL::asset('assets/article_files/'.$article->link)}}">Download full article</a></div>
				</div>
			<?php if($count == 1 || $count == count($articles)) {?>
			</li>
			<?php $count = -1; } ?>
		<?php $count++; } ?>
		</ul>
	</div>
</div>

</div>


<!-- slider end-->
@endsection