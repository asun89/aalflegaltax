@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/about-banner.jpg')}}) !important">
 <div class="container about-us-wording">

 	<div id="about-content">
	    <div class="row">
	    	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
		        <div class="banner-title about-right">
					<h1>Idealism and Determination to bring a real contribution to the development of the world of Law in Indonesia.</h1>
				</div>
		    </div>
		    <div class="col-lg-5 col-md-5 col-sm-0 col-xs-0">
		    </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title about-left">
				<h1 class="desktop">ABOUT<br/>US</h1>
				<h1 class="mobile">ABOUT US</h1>
			</div>
	      </div>
	      
	    </div>
	</div>

    
  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/about-banner.jpg')}}) !important">
 <div class="container about-us-wording">

 	<div id="about-content-mobile">
	    <div class="row">
	    	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
		        <div class="banner-title about-right">
					<h1>Idealism and Determination to bring a real contribution to the development of the world of Law in Indonesia.</h1>
				</div>
		    </div>
		    <div class="col-lg-5 col-md-5 col-sm-0 col-xs-0">
		    </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title about-left">
				<h1 class="desktop">ABOUT<br/>US</h1>
				<h1 class="mobile">ABOUT US</h1>
			</div>
	      </div>
	      
	    </div>
	</div>

    
  </div>
</div>
<!--  Banner Area End here -->

<!-- Experience Area Start here -->
<div class="about-content">
  <div class="container">
	<div class="row about-section top-about hide_it_first">
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 <div class="about-section-1-title"><h2>AALF Legal & Tax Consultants</h2></div>
		 <div class="border-under-title-left desktop"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
		 <div class="about-section-1-content mobile">
		 	<ul class="lightSlider about-content-slider">
		 		<li>
				 	For approximately more than 20 years working and actively participating in the development of law in Indonesia, AALF has been able to build a broad and comprehensive network with private legal institutions and the government as regulators and those who have authority. As a form, currently AALF has succeeded in establishing a close relationship based on ethics and professionalism with the courts, prosecutors and thepolice..<br/><br/>

					In the next stage, by looking at finance and taxation as two fields that cannot be separated from legal issues and in order to serve clients more optimally, there is also a division that will specifically provide services to resolve these problems.<br/><br/>
		  			
				</li>
		  		<li>
				  Tax professionals at AALF have substantial
				experience in all tax matters with high level of
				knowledge and dedication to remain up to date
				with constant changes and to adapt with new
				challenges. Our team comprise of individuals
				with professional certification and licenses, i.e.,
				tax consultant certification C (the highest working
				license qualification), Tax Court attorney license,
				and customs clearance license.<br/><br/>

		  		</li>
		  		<li>
				  We have served a broad range of national and
					multinational companies in Indonesia engage in
					various industries or business lines, as well as
					extensively represented tax dispute and litigation
					(audit, investigation, objection, appeal/lawsuit,
					judicial review/counter memorandum of judicial
					review), tax advisory, tax compliance, transfer
					pricing, international tax/cross border transac
					tions, strategic tax planning, tax due diligence, tax
					diagnostic review, obtaining tax facilities &
					rulings, accounting services, payroll services, etc.<br/><br/>

					We are confident of our service capabilities.
					Therefore, we look forward for the opportunity
					to demonstrate how we are able to provide legal
					and tax services and what we can do for the
					client. 
		  		</li>
	      	</ul>
		 </div>

		 <div class="about-section-1-content desktop">

		 	<div class="about-section-1-content-left">
			 For approximately more than 20 years working and actively participating in the development of law in Indonesia, AALF has been able to build a broad and comprehensive network with private legal institutions and the government as regulators and those who have authority. As a form, currently AALF has succeeded in establishing a close relationship based on ethics and professionalism with the courts, prosecutors and thepolice..<br/><br/>

			In the next stage, by looking at finance and taxation as two fields that cannot be separated from legal issues and in order to serve clients more optimally, there is also a division that will specifically provide services to resolve these problems.<br/><br/>
		 	</div>
		 	<div class="about-section-1-content-right">
			 	Tax professionals at AALF have substantial
				experience in all tax matters with high level of
				knowledge and dedication to remain up to date
				with constant changes and to adapt with new
				challenges. Our team comprise of individuals
				with professional certification and licenses, i.e.,
				tax consultant certification C (the highest working
				license qualification), Tax Court attorney license,
				and customs clearance license.<br/><br/>

				We have served a broad range of national and
				multinational companies in Indonesia engage in
				various industries or business lines, as well as
				extensively represented tax dispute and litigation
				(audit, investigation, objection, appeal/lawsuit,
				judicial review/counter memorandum of judicial
				review), tax advisory, tax compliance, transfer
				pricing, international tax/cross border transac
				tions, strategic tax planning, tax due diligence, tax
				diagnostic review, obtaining tax facilities &
				rulings, accounting services, payroll services, etc.<br/><br/>

				We are confident of our service capabilities.
				Therefore, we look forward for the opportunity
				to demonstrate how we are able to provide legal
				and tax services and what we can do for the
				client. 
		 	</div>
		 </div>

	   </div>

	</div>

	<div class="row about-section bottom-about hide_it_first">
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 <div class="about-section-1-title-center"><h2>Our History</h2></div>
		 <div class="border-under-title desktop"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
		 <div class="about-section-1-content margin-under-title desktop">

		 	<div class="about-section-1-content-left about-img-container">
		 		<img src="{{URL::asset('assets/img/about-content.jpg')}}" />
		 	</div>
		 	<div class="about-section-1-content-right about-text-right">
		 		2002: Mid-May located in Menara Batavia Jakarta, AALF initially founded by two idealist and ambitious young lawyers who have aspirations to devote themselves and their abilities to advancing the world of law.<br/><br/>

				2009: AALF moved its offices to the Indonesia Stock Exchange as an anticipation of development of AALF’s business that is progressing rapidly. Also on that year, recruited Marcella Santoso as a new partner.<br/><br/>

				2013: AALF moved its offices to The Equity Tower, a very prestigious business area located in the central business district of Jakarta. The survey stated that only companies with admirable capabilities could be based in this region.<br/><br/>

				2017: AALF is growing increasingly with more clients being served, assets being managed and associates being hired. In this year AALF began to reorganize by improving the organizational structure, system and governance and also recruitment pattern.<br/><br/>

				2019: AALF is growing increasingly with more clients being served, assets being managed and associates being hired. In this year AALF began to reorganize by improving the organizational structure, system and governance and recruitment pattern as well. This year, AALF also developed itself by forming financial and taxation divisions with the aim of serving clients optimally.

		 	</div>
		 </div>

		 <div class="about-section-1-content margin-under-title mobile">
		 	<ul class="lightSlider about-content-slider">
		 		<li>
		 			<div class="about-section-1-content-left about-img-container">
				 		<img src="{{URL::asset('assets/img/about-content.jpg')}}" />
				 	</div>
		 			<div class="about-section-1-content-right">
		 			2002: Mid-May located in Menara Batavia Jakarta, AALF initially founded by two idealist and ambitious young lawyers who have aspirations to devote themselves and their abilities to advancing the world of law.<br/><br/>

					2009: AALF moved its offices to the Indonesia Stock Exchange as an anticipation of development of AALF’s business that is progressing rapidly. Also on that year, recruited Marcella Santoso as a new partner.<br/><br/>

					2013: AALF moved its offices to The Equity Tower, a very prestigious business area located in the central business district of Jakarta. The survey stated that only companies with admirable capabilities could be based in this region.<br/><br/>
					</div>
		  		</li>
		  		<li>
		  			<div class="about-section-1-content-right">
		 			2017: AALF is growing increasingly with more clients being served, assets being managed and associates being hired. In this year AALF began to reorganize by improving the organizational structure, system and governance and also recruitment pattern.<br/><br/>

					2019: AALF is growing increasingly with more clients being served, assets being managed and associates being hired. In this year AALF began to reorganize by improving the organizational structure, system and governance and recruitment pattern as well. This year, AALF also developed itself by forming financial and taxation divisions with the aim of serving clients optimally.
		  		</li>
	      	</ul>
		 </div>


	   </div>

	</div>
  </div>
</div>
<!-- Experience Area End here -->

<!-- slider end-->
@endsection