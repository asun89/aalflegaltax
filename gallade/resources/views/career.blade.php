@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/career-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="career-content">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Work Ethics is<br/>the tradition of AALF culture </h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>CAREER</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/career-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="career-content-mobile">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Work Ethics is<br/>the tradition of AALF culture </h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>CAREER</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area career-aalf">
 <div class="container">
   <div class="row vacancy-container">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="section-title-area-left">
     <h2>{{trans('messages.vacancy_title')}}</h2>
     </div>
     <div class="border-under-title-area-left margin-for-border"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
     <div class="section-description-area-justify letter-spacing-1-px">Lawyers at AALF are grouped into several levels based on competency and experience ranging from Junior Associates, Associates, Senior Associates and Advocates who are all synergized to work together continuously in solving various legal cases effectively and comprehensively in Head Legal System (HLS).

    </div>
   </div>
   </div>
 </div>
</div>

<div class="career-list-section">
<div class="container">
   <div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 career-list-container">
    
<?php $count = 0; foreach($jobs as $job) { ?>
      <div target="req<?php echo $job->id; ?>" state="closed" class="career-list <?php if($count == 0) {echo "career-first";} ?>" style="overflow: hidden;">
        <div class="career-list-item" style="overflow: hidden;">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 career-job-title">
            <div class="job-title">
              <a href="#" class="vacancy_link" onclick="return false">
                <div class="arrow-img">
                  <img class="arrow" src="{{URL::asset('assets/img/career_arrow_closed.png')}}" alternate-src="{{URL::asset('assets/img/career_arrow_opened.png')}}"> 
                </div>
                <div class="job-title-wording">
                  <?php echo $job->job_title; ?>
                </div>
              </a>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 <?php if($job->slot > 0){echo 'career-right-part'; }else{ echo 'career-right-part-no-vacancy';}  ?>">
            <?php if($job->slot > 0){ ?>
              <?php echo $job->slot; ?> {{trans('messages.open_position')}}
            <?php } else { ?>
              {{trans('messages.no_vacancy')}}
            <?php } ?>
          </div>
        </div>

        <div class="career-requirements" id="req<?php echo $job->id; ?>">
          {{trans('messages.requirements')}}:<br/>
          <ul>
            <?php $requirements = json_decode($job->requirements); 
            foreach($requirements as $req) { ?>
              <li>- <?php echo $req; ?></li>
            <?php } ?>
          </ul><br/>

          {{trans('messages.send_application_wording')}} <a href="mailto:info@aalflegaltax.com">info@aalflegaltax.com</a>
        </div>
      </div>
<?php $count++; } ?>
      
    </div>
  </div>
  </div>
</div>
 </div>
</div>


<!-- slider end-->
@endsection