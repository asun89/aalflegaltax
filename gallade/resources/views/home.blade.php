@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!-- slider -->
<!--  Banner Area Start here -->

<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/home-banner.jpg')}}) !important">
 <div class="container">
    <div class="row">
    	<div class="col-lg-7 col-md-7 col-sm-0 col-xs-0"></div>
    	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    		<div class="banner-title home" id="fadein-home-banner">
    			<h1>Provide the best sepurposervices in order to build trust</h1>
         		<a href="{{ URL::to('about') }}" class="banner-button">
         			<div>Read more</div>
         		</a>
    		</div>
    	</div>
    </div>
  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/home-banner.jpg')}}) !important">
 <div class="container">
    <div class="row">
    	<div class="col-lg-7 col-md-7 col-sm-0 col-xs-0"></div>
    	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    		<div class="banner-title home" id="fadein-home-banner-mobile">
    			<h1>Provide the best sepurposervices in order to build trust</h1>
         		<a href="{{ URL::to('about') }}" class="banner-button">
         			<div>Read more</div>
         		</a>
    		</div>
    	</div>
    </div>
  </div>
</div>


<!--  Banner Area End here -->
<!-- slider end-->
<!-- Experience Area Start here -->
<div class="experince-area">
  <div class="container">
	<div class="row">
		 <div class="experince">
		  	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		  		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" id="welcome-home-container">
			  		<div id="welcome-to-aalf-legal-tax-home">
				  		<span id="welcome-to-home">"Welcome to <br/>
				  		<span id="aalf-home">AALF</span><br/>
				  		<span id="legal-tax-home">Legal & Tax Consultants"</span>
			  		</div>
			  		<div id="welcome-to-aalf-legal-tax-home-mobile">
				  		<span id="welcome-to-home">"Welcome to<br/>
				  		<span id="legal-tax-home"><span id="aalf-home">AALF</span> Legal & Tax Consultants"</span>
			  		</div>
		  		</div>
		  		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 home-line">
			  		<img width="1.85" src="{{URL::asset('assets/img/home-line.svg')}}"/>
			  	</div>
		  	</div>
		  	
		  	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="center-text-home">
		  		<div id="home-content-1">
		  			Our main goal is to serve our client by providing legal services with a holistic perspective from the corporate side including financial and taxation issues. Therefore, we always strive to provide comprehensive solution in order to protect the interests of our clients, as optimal as possible, especially if the problem intersects with many aspects.
		  		</div>
		  	</div>
		  	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="right-text-home">
		  		<div id="home-content-2">
		  			Finance and taxation are two fields that cannot be separated from legal issues. In order to answer these challenges and to be able to serve clients more optimally, there is also a division that will specifically provide services to resolve these problems.
		  		</div>
		  	</div>
		 </div>
	   </div>
  </div>
</div>
<!-- Experience Area End here -->
<!-- Our Practice Area Start Here -->
<div class="our-practice-area">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.area_of_expertise')}}</h2>
	   </div>
	   <div class="border-under-title" id="home-border-under"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	   <div class="section-description-area" id="home-description">We humbly convey that we have all the above-average capabilities to serve in the fields of law, finance and taxation. Therefore, we look forward for the opportunity to demonstrate how we work and serve in these fields.</div>
	 </div>
   </div>
 <div class="practice-area hide_it_first" id="expertise-area">
   <div class="row">

	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">

	  	<a href="http://www.ariyantoarnaldo.com" target="_blank" class="expertise-icon desktop-aoe">
		  	<div class="area-of-expertise-block">
		  		<div class="aoe-icon">
		  			<img class="expertise-img" src="{{URL::asset('assets/img/business_law.svg')}}" alternate-src="{{URL::asset('assets/img/business_law_white.svg')}}"/>
		  		</div>
		  		<div class="aoe-title">Business Law</div>
		  		<div class="aoe-description"><span class="gold-aalf">AALF Legal & Tax Consultants</span> brings a deep understanding of Indonesian law, both civil and criminal procedural law, taxation regulation and familiar with best practices in financial area. In addition, we always serve with high standards of professionalism, uphold ethical values and always refer to the rules/regulations.</div>
		  		<div class="aoe-readmore">Read more <img class="arrow-img-home" alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></div>
		  	</div>
	  	</a>

	  	<a href="http://www.ariyantoarnaldo.com" target="_blank" class="expertise-icon mobile-aoe">
	  		<div class="aoe-icon">
	  			<img class="expertise-img" src="{{URL::asset('assets/img/business_law.svg')}}" alternate-src="{{URL::asset('assets/img/business_law_white.svg')}}"/>
	  		</div>
	  		<div class="aoe-title">Business Law</div>
	  		<div class="aoe-description"><span class="gold-aalf">AALF Legal & Tax Consultants</span> brings a deep understanding of Indonesian law, both civil and criminal procedural law, taxation regulation and familiar with best practices in financial area. In addition, we always serve with high standards of professionalism, uphold ethical values and always refer to the rules/regulations.</div>
	  		<div class="aoe-readmore">Read more <img class="arrow-img-home" alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></div>
	  	</a>

	  </div>
	
	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">
		<a href="{{ URL::to('about') }}" class="expertise-icon desktop-aoe">
			<div class="area-of-expertise-block">
		  		<div class="aoe-icon">
		  			<img class="expertise-img" src="{{URL::asset('assets/img/legal_tax.svg')}}" alternate-src="{{URL::asset('assets/img/legal_tax_white.svg')}}" />
		  		</div>
		  		<div class="aoe-title">Legal & Tax</div>
		  		<div class="aoe-description"><span class="gold-aalf">AALF Legal & Tax Consultants</span> has served a broad range of Indonesian companies engage in various industries or business lines. We have extensively represented tax dispute and litigation (audit, objection, appeal/lawsuit, judicial review and counter memorandum of judicial review), strategic tax planning, tax diagnostic review, tax due diligence, transfer pricing documentation, annual and monthly tax compliance, cross border transaction, and obtaining tax facilities and rulings.</div>
		  		<div class="aoe-readmore">Read more <img class="arrow-img-home" alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></div>
		  	</div>
	  	</a>

	  	<a href="{{ URL::to('about') }}" class="expertise-icon mobile-aoe">
			<div class="aoe-icon">
	  			<img class="expertise-img" src="{{URL::asset('assets/img/legal_tax.svg')}}" alternate-src="{{URL::asset('assets/img/legal_tax_white.svg')}}" />
	  		</div>
	  		<div class="aoe-title">Legal & Tax</div>
	  		<div class="aoe-description"><span class="gold-aalf">AALF Legal & Tax Consultants</span> has served a broad range of Indonesian companies engage in various industries or business lines. We have extensively represented tax dispute and litigation (audit, objection, appeal/lawsuit, judicial review and counter memorandum of judicial review), strategic tax planning, tax diagnostic review, tax due diligence, transfer pricing documentation, annual and monthly tax compliance, cross border transaction, and obtaining tax facilities and rulings.</div>
	  		<div class="aoe-readmore">Read more <img class="arrow-img-home" alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></div>
	  	</a>

	  </div>
	

	</div>
 </div>
</div>
 </div>
</div>
<!-- Our Attorney End Here -->
<!-- Latest News Area Start Here -->
<div class="latest-news-area fifteen-years-experience years_of_experience">
<div class="container">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="exp-counter">0</div>
		<div class="exp-wording">
			<div class="border-gold-mobile"></div>
			<div class="title">22 Years of Experience and Success</div>
			<div class="title-mobile"><span class="heading-title">22 Years</span><br/>	of Experience and Success</div>
			<div class="border-gold"></div>
			<div class="description">
				After the financial crisis and political transition in 1998, rapid changes in banking, bankruptcy, capital markets and foreign investment regulations are continuously occurred and new reforms in Company and Commercial laws. AALF in respect of the aforementioned issues, always deeply understand the laws, the people and the cultures of Indonesia and most importantly has been loyally committed to its clients despite the prevailing social, political and economic conditions and uncertainties in the future. <br/><br/>
				Starting from merely two lawyers, over a period of more than one and half decades AALF has grown and developed into an institution that is trusted by the public and the legal community at the regional level in Indonesia, especially Jakarta. There are currently around 50 lawyers from various legal education backgrounds graduating from leading universities in Indonesia, mostly from the Faculty of Law, University of Indonesia.
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 contact-form-box desktop">
		<div class="title">Contact Us</div>
		<div class="form-container">
			<form action="{{ URL::to('contact/submit') }}" method="POST" id="contact_form">
				{{ csrf_field() }}
				<div class="form-title">NAME</div>
				<div class="form-field"><input type="text" name="name" required/></div>
				<div class="form-title">E-MAIL</div>
				<div class="form-field"><input type="email" name="email" required/></div>
				<div class="form-title">PHONE</div>
				<div class="form-field"><input type="number" name="phone" required/></div>
				<div class="form-title">SUBJECT</div>
				<div class="form-field"><input type="text" name="subject" required/></div>
				<div class="form-title">YOUR MESSAGE</div>
				<div class="form-field-textarea"><textarea name="message" required></textarea></div>
				<div class="form-btn-submit">
					<button class="get-a-consultant-btn">GET A CONSULTANT</button>
				</div>
			</form>
		</div>
		
	</div>
</div>
<div class="container contact-form-box mobile">
		<div class="title">Contact Us</div>
		<div class="form-container">
			<form action="{{ URL::to('contact/submit') }}" method="POST" id="contact_form">
				{{ csrf_field() }}
				<div class="form-title">NAME</div>
				<div class="form-field"><input type="text" name="name" required/></div>
				<div class="form-title">E-MAIL</div>
				<div class="form-field"><input type="email" name="email" required/></div>
				<div class="form-title">PHONE</div>
				<div class="form-field"><input type="number" name="phone" required/></div>
				<div class="form-title">SUBJECT</div>
				<div class="form-field"><input type="text" name="subject" required/></div>
				<div class="form-title">YOUR MESSAGE</div>
				<div class="form-field-textarea"><textarea name="message" required></textarea></div>
				<div class="form-btn-submit">
					<button class="get-a-consultant-btn">GET A CONSULTANT</button>
				</div>
			</form>
		</div>
		
	</div>
</div>
<!-- Latest News Area End Here -->
@endsection
<?php if($loaded != "true") {?>
<style>
	#website-content{
	  opacity: 0;
	}

	#website-loading {
	  background: #C6AC71;
	  width:100%;
	  height:100%;
	  z-index: 999999999;
	  position: fixed;
	}

	#logo-intro{
		margin-bottom:20px;
	}
</style>
<div id="website-loading">
	<div style="padding:150px 80px; height: 400px; text-align: center; top:50%; left:50%;position: fixed; transform: translate(-50%, -50%);">
	    <img id="logo-intro" src="{{URL::asset('assets/img/logo-intro.png')}}">
	    <div id="maxloadingbar" style="border:1px solid #FFFFFF; z-index: 0; position: relative;">
	    </div>
	    <div id="loadingbar" style="border:1px solid #808080; z-index: 100000; position: relative; width:10%; margin: auto; margin-top:-2px;">
	    </div>
	</div>
</div>
<?php } ?>
