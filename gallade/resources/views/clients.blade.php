@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/client-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="client-content">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Consultation, Mapping of Legal Efforts and<br/>Implementation of Legal Efforts</h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>CLIENTS</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/client-banner.jpg')}}) !important">
 <div class="container">
    
    <div id="client-content-mobile">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="banner-title expertise-right">
          <h1>Consultation, Mapping of Legal Efforts and<br/>Implementation of Legal Efforts</h1>
        </div>
          </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="banner-title expertise-left">
        <h1>CLIENTS</h1>
      </div>
        </div>
        
      </div>
    </div>


  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area">
 <div class="container">
   <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="section-title-area-clients">
     <h2>{{trans('messages.thanks_for_trust')}}</h2>
     </div>
     <div class="border-under-title margin-after-border"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
   </div>
   </div>
</div>
</div>

<div class="contact-location">
  <div class="container">
   <div class="row">

  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 section-description-area">
    We have team that has various experience in assisting and representing local and multinational companies, including major plantation companies, major oil & gas companies, property & real estate companies, leading bank companies, major mining companies, construction companies, leading sugar industry, consumer product companies, major timber companies, manufacturing companies, trading companies, funeral home, outsourcing companies and other service companies.
  </div>
  <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
 </div>
</div>
</div>

<div class="clients-logo-section" style="display: none;">
   <div class="container">
      <ul id="lightSlider">
        <?php $clients_count = count($clients);
        $num_slides = ceil($clients_count / 12);
        $c = 0;
          for($a = 0; $a < $num_slides; $a++){
            echo '<li>';

            for($b = 0; $b < 12; $b++){
              ?>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 client-logo" 
              style="<?php 
              if(in_array($b, array(0,1,2))){
                echo 'border-left:none; border-top:none;';
              } else if(in_array($b, array(3))){
                echo 'border-left:none; border-top:none; border-right: none';
              } else if(in_array($b, array(4,5,6))){
                echo 'border-left:none; border-bottom:none; border-top: none';
              } else if(in_array($b, array(8,9,10))){
                echo 'border-left:none; border-bottom:none;';
              } else if(in_array($b, array(7))){
                echo 'border-left:none; border-bottom:none; border-right:none; border-top:none;';
              } else if(in_array($b, array(11))){
                echo 'border-left:none; border-bottom:none; border-right:none;';
              }
              $c++;

              ?>">
              <?php if($c < ($clients_count)){ ?>
                <img src="{{URL::asset('assets/img/clients/'.$clients[$c]->image)}}">
              <?php }?>
              </div>
              <?php 
            }

            echo '</li>';
          }
        ?>
      </ul>

   </div>
 </div>


</div>


<!-- slider end-->
@endsection