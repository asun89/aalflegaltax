@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/team-banner.jpg')}}) !important">
 <div class="container">
    <div id="team-content">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Solving various legal cases<br/>effectively and comprehensively</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>TEAM</h1>
			</div>
	      </div>
	      
	    </div>
	</div>

  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/team-banner.jpg')}}) !important">
 <div class="container">
    <div id="team-content-mobile">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Solving various legal cases<br/>effectively and comprehensively</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>TEAM</h1>
			</div>
	      </div>
	      
	    </div>
	</div>

  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area team-aalf">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area-left">
		 <h2>{{trans('messages.teams_title')}}</h2>
	   </div>
	   <div class="border-under-title-area-left margin-for-border"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	   <div class="section-description-area-justify">Currently, there are around 50 lawyers with various legal education backgrounds from leading universities in Indonesia affiliated with AALF. They are spread in Jakarta and Surabaya with the task of completing various legal cases that are being handled by AALF. About 30 lawyers who are almost all of them alumni of the Faculty of Law, University of Indonesia, are permanently domiciled at the AALF Jakarta office, namely Equity Tower and partly in Revenue Tower.
		</div>
	 </div>
   </div>
 </div>
</div>

<div class="team-list-name">
<div class="container">
   <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 desktop">

		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
			<?php foreach($teams as $team) { ?>
			  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">
		  	  	<div class="team-page-block">
			  		<div class="team-title">
			  			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="team-page-block-title"><?php echo $team->title; ?></div>
			  		</div>
			 		<div class="team-page-border-under-title-left-small"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
			  		
			  		<div class="team-page-list">

			  			<?php $members = json_decode($team->content);
			  			$count = 0;
			  			foreach($members as $member){
			  			?>
				  			<div class="team-page-list-content <?php if($count == (count($members) - 1)){ echo 'team-page-list-last'; }?>" <?php if($count == 0){ echo 'id="top-list-content"'; }?>>
				  				<?php echo $member; ?>
				  			</div>
			  			<?php $count++; } ?>
			  		</div>
			  	</div>
			  </div>
			<?php } ?>
		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile">
	  <?php foreach($teams as $team) { ?>
		  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">
		  	<div class="mobile-page-icon">
		  	  	<div class="mobile-icon">
		  			<img class="expertise-img" src="{{URL::asset('assets/img/'.$team->image)}}" />
		  		</div>
		  		<div class="mobile-title">
			  		<?php echo $team->title; ?>
		  		</div>
	  		</div>
	  		
	  		<div class="mobile-page-list">
	  			<ul class="lightSlider">
		  			<?php $members = json_decode($team->content);
			  			$a = 0;
			  			foreach($members as $member){
		  				if($a == 0){
			  				echo "<li>";
			  			}
		  				?>
			  			<div class="aoe-page-list-content <?php if($a == 0){echo 'first-row-content';}?>">
			  				<?php echo $member; ?>
			  			</div>
		  			<?php 
		  				if($a == 8){
			  				echo "</li>";
		  					$a = -1;
			  			} 
			  		$a++;} ?>
		  		</ul>
	  		</div>
		  </div>
	<?php } ?>
	  
	</div>

	</div>
</div>
 </div>
</div>


<!-- slider end-->
@endsection