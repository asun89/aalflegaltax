@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/expertise-banner.jpg')}}) !important">
 <div class="container">

 	<div id="expertise-content">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Experienced practitioners with<br/>multi disciplinary backgrounds</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>EXPERTISE</h1>
			</div>
	      </div>
	      
	    </div>
	</div>


  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/expertise-banner.jpg')}}) !important">
 <div class="container">

 	<div id="expertise-content-mobile">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Experienced practitioners with<br/>multi disciplinary backgrounds</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>EXPERTISE</h1>
			</div>
	      </div>
	      
	    </div>
	</div>


  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area">
		 <h2>{{trans('messages.area_of_expertise')}}</h2>
	   </div>
	   <div class="border-under-title"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	   <div class="section-description-area">We are confident of our service capabilities. Therefore we look forward for the opportunity to demonstrate how we are able to provide law, legal and tax services.</div>
	 </div>
   </div>
 <div class="practice-area" id="expertise-area">
   <div class="row">

	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 desktop">
	  
	  <?php foreach($expertises as $expertise) { ?>
	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">
  	  	<div class="aoe-page-block">
  	  		<a show-target="<?php echo $expertise->id; ?>" state="0" class="expertise-toggle" href="#">
	  		<div class="aoe-page-icon">
	  			<img class="expertise-img" src="{{URL::asset('assets/img/'.$expertise->image)}}" />
	  		</div>
	  		<div class="aoe-page-title">
		  			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" id="aoe-page-left-title"><?php echo $expertise->title; ?></div>
		  			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" id="aoe-page-right-title">+</div>
	  		</div>
	  		</a>
	 		<div class="aoe-page-border-under-title-left-small"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	  		
	  		<div class="aoe-page-list" id="<?php echo $expertise->id; ?>">
	  			<?php $content = json_decode($expertise->content); ?>
	  			<?php $a = 0; foreach($content as $c) { ?>
		  			<div class="aoe-page-list-content <?php if($a == (count($content) - 1)){echo 'aoe-page-list-last';}?>" <?php if($a == 0){echo 'id="top-list-content"';}?>>
		  				<?php echo $c; ?>
		  			</div>
	  			<?php $a++; } ?>
	  		</div>
	  	</div>
	  </div>
	<?php } ?>
	</div>


	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mobile">
	  <?php foreach($expertises as $expertise) { ?>
		  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 area-of-expertise-container" style="overflow: hidden;">
		  	<div class="mobile-page-icon">
		  	  	<div class="mobile-icon">
		  			<img class="expertise-img" src="{{URL::asset('assets/img/'.$expertise->image)}}" />
		  		</div>
		  		<div class="mobile-title">
			  		<?php echo $expertise->title; ?>
		  		</div>
	  		</div>
	  		
	  		<div class="mobile-page-list">
	  			<ul class="lightSlider">
		  			<?php $content = json_decode($expertise->content); ?>
		  			<?php $a = 0; foreach($content as $c) { 
		  				if($a == 0){
			  				echo "<li>";
			  			}
		  				?>
			  			<div class="aoe-page-list-content <?php if($a == 0){echo 'first-row-content';}?>">
			  				<?php echo $c; ?>
			  			</div>
		  			<?php 
		  				if($a == 8){
			  				echo "</li>";
		  					$a = -1;
			  			} 
			  		$a++;} ?>
		  		</ul>
	  		</div>
		  </div>
	<?php } ?>
	  
	</div>



 </div>
</div>

 </div>
</div>


<!-- slider end-->
@endsection