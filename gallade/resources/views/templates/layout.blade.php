<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Ariyanto Arnaldo Legal Tax Consultants - <?php echo $meta_title; ?></title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="<?php echo $meta_description; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('assets/img/favicon.png')}}">	
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css')}}">
        
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/lightslider/css/lightslider.min.css')}}" type="text/css" />
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/owl.transitions.css')}}">
		<!-- jquery-ui CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery-ui.css')}}">
		<!-- meanmenu CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/meanmenu.min.css')}}">
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css')}}">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css')}}">     
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/nivo-slider.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('assets/custom-slider/css/preview.css')}}" type="text/css" media="screen" />
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css')}}">
    <!-- aalf CSS
    ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/aalf.css?4')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/aalf-xs.css?4')}}">
    <!-- Responsive CSS
    ============================================ -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/responsive.css')}}">

        <!-- CSS -->
        <link rel="stylesheet" href="{{ URL::asset('assets/sweetalert2/sweetalert2.min.css')}}"/>
        <!-- Bootstrap theme -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}"/>

        <link rel="stylesheet" href="{{ URL::asset('assets/css/fontawesome/css/all.min.css')}}"/>

        @foreach($css_files as $cf)
          <link rel="stylesheet" href="{{URL::asset($cf)}}" />
        @endforeach

		<!-- modernizr JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body class="{{$themes}}">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Header Area Start Here -->
        <header>
        	<div class="header-area-top-area">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
        				</div>
        				<div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
        					<div class="header-top-right">
        						<nav>
                      <ul>
                        <li><a href="{{url()->current()}}?lang=en" class="<?php if(Config::get('app.locale') == "en"){echo 'current-page';}?>">EN</a></li>
                        <li><a href="{{url()->current()}}?lang=id" class="<?php if(Config::get('app.locale') == "id"){echo 'current-page';}?>">ID</a></li>
                      </ul>
                    </nav>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
           <div class="main-header-area" style="background:#FFFFFF;">
                <div class="container">
                     <div class="row">                         
                          <div class="col-lg-3 col-md-3 col-sm-12 col-sm-12">
                              <div class="logo-area">
                                  <a href="{{ URL::to('home') }}"><img src="{{ URL::asset('assets/img/logo.svg?3')}}" alt="logo"></a>
                              </div>
                          </div>  
                          <div class="col-lg-9 col-md-9 col-sm-12 col-sm-12 main-menu-position">
                              <div class="main-menu-area">
                                  <nav>
                                      <ul>
                                          <li><a class="<?php if(Request::path() == "about"){echo 'current-page';}?>" href="{{ URL::to('about') }}">{{trans('messages.about_us')}}</a></li>
                                          <li><a class="<?php if(Request::path() == "expertises"){echo 'current-page';}?>" href="{{ URL::to('expertises') }}">{{trans('messages.expertises')}}</a></li>
                                          <li><a class="<?php if(Request::path() == "partners"){echo 'current-page';}?>" href="{{ URL::to('partners') }}">{{trans('messages.partners')}}</a></li>
                                          <li><a class="<?php if(Request::path() == "teams"){echo 'current-page';}?>" href="{{ URL::to('teams') }}">{{trans('messages.teams')}}</a></li>
                    										  <li><a class="<?php if(Request::path() == "career"){echo 'current-page';}?>" href="{{ URL::to('career') }}">{{trans('messages.career')}}</a></li>
                                          <li><a class="<?php if(Request::path() == "clients"){echo 'current-page';}?>" href="{{ URL::to('clients') }}">{{trans('messages.clients')}}</a></li>
                                          <li><a class="<?php if(in_array(Request::path(), array("article"))){echo 'current-page';}?>" href="{{ URL::to('article') }}">{{trans('messages.article')}}</a></li>
                                          <li><a class="<?php if(Request::path() == "contact"){echo 'current-page';}?>" href="{{ URL::to('contact') }}">{{trans('messages.contact_us')}}</a></li>
                                      </ul>
                                  </nav>
                              </div>
                          </div> 
                     </div>
                 </div> 
            </div>
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
              <div class="container">

              <div class="mobile-logo"><a href="{{ URL::to('home') }}"><img src="{{ URL::asset('assets/img/logo.svg')}}" alt="logo"></a></div>
                
                <div class="col-xs-6">
                  <div class="mobile-menu">



                    <nav id="dropdown">
                      <ul>
                        <li><a class="<?php if(in_array(Request::path(), array("home", "","/"))){echo 'current-page';}?>" href="{{ URL::to('home') }}">{{trans('messages.home')}}</a></li>
                        <li><a class="<?php if(Request::path() == "about"){echo 'current-page';}?>" href="{{ URL::to('about') }}">{{trans('messages.about_us')}}</a></li>
                        <li><a class="<?php if(Request::path() == "expertises"){echo 'current-page';}?>" href="{{ URL::to('expertises') }}">{{trans('messages.expertises')}}</a></li>
                        <li><a class="<?php if(Request::path() == "partners"){echo 'current-page';}?>" href="{{ URL::to('partners') }}">{{trans('messages.partners')}}</a></li>
                        <li><a class="<?php if(Request::path() == "teams"){echo 'current-page';}?>" href="{{ URL::to('teams') }}">{{trans('messages.teams')}}</a></li>
                        <li><a class="<?php if(Request::path() == "career"){echo 'current-page';}?>" href="{{ URL::to('career') }}">{{trans('messages.career')}}</a></li>
                        <li><a class="<?php if(Request::path() == "clients"){echo 'current-page';}?>" href="{{ URL::to('clients') }}">{{trans('messages.clients')}}</a></li>
                        <li><a class="<?php if(Request::path() == "article"){echo 'current-page';}?>" href="{{ URL::to('article') }}">{{trans('messages.article')}}</a></li>
                        <li><a class="<?php if(Request::path() == "contact_us"){echo 'current-page';}?>" href="{{ URL::to('contact') }}">{{trans('messages.contact_us')}}</a></li>
                      </ul>
                    </nav>



                  </div>          
                </div>
              </div>
              </div>
            </div>
          </div>
            <!-- mobile-menu-area end -->
        </header>
        <!-- Header Area End Here --> 
		@yield('content')
      
      <!-- Footer Area Start Here -->
      <footer>
        <div class="footer-top-area">
          <div class="container">
            <div class="back_to_top" id="backtotop_2">
              <a href="#" class="scrollup"><img src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABUCAYAAAAMC14dAAABuUlEQVRoge2XwXHCMBBFP1SQEkgH5MqJdJADBaQEVAFJBS4hDWgmJSQnXZMOoARKYEx2HWAlW7IxXP67eEaWzWP12VkwlODd19B3TM1KAcG7NwBLufZmMkBgBuAHwAOAPYCnxaramY0jV6ISAci1Mjsy6VWJ4N0LgE9zA3herKpvs9pBcSWCd23f+sOsjCEBYA1gZlb/mPUJadFxSBi35oblsSSkpZXILXfRsWRLBO9e655gbsRZSnizyDoOCeP25CeZw056x75rb24lNoUCkPCuzWqEzkoE7+bSGftSV+O37dmcSqR6Qi6dz7dKBO/WBWFMsZRQJ0keR88wpthL74iGtK0S1ZUEIO/ZmFUhWongXX0Eg4eVCNGQpioxNIwpop3USEgY52bndZjL+884O46LaWksTEgvK3HNMKYw80hTiRHDmKKZwk4rEQ3NiDSfN8X/6J6alsaimcImNwpjiuNfhemNwpjiGNJox+xCyhhrw++LVVU86JpmdQ8ooVBCoYRCCYUSCiUUSiiUUCihUEKhhEIJhRIKJRRKKJRQKKFQQqGEQgmFEgolFEoolFAooVBCoYRyfwkAB8ZcZ2KRdqBxAAAAAElFTkSuQmCC"/> BACK TO TOP</a>
            </div>
            <div class="back_to_top" id="backtotop_1">
              <a href="#" class="scrollup"><img alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABUCAYAAAAMC14dAAABfklEQVRoge2W23HCMBREpVTgEkwHpIOUQAmUQAe4g5QCHaQEl+B0EDrYjCZaAr6yxw/J/Oz5YUaWrs6gHem6tQD4Wl1kDQAa/NG8SqAG8BMlwm9tJm0gccEzFzOpsMABaT7M5EICFYAuqQB0ZkEhicZs/UzZkMYwTqFcSMOdMFGizN0B4Gi2GudgiqwUqB7uhKmE8FamWII3O5Tk7JybVPCBkIuTGV0CgP3Mf6DPPofE1DAOsS6kAE4DhedyNMUf8GbkXyBkoFuQhRQ359zOe39LfBsN5mcmARfrnM3oGOEhynQMfaaHFEBrluehNZulyBjGIczd8RTM+PC0GbOQwoS0H8ycYRyiivtYCoZxiHsXdj+O2BVt2ax+e+93jscRu6Gtu+WaXZjfKIxDhHC++9iq521A5nFdtGqk2V3U5I69HZshCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgkiCSIJIgnyegnn3C95AOyU+WrSMgAAAABJRU5ErkJggg==" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAABUCAYAAAAMC14dAAABuUlEQVRoge2XwXHCMBBFP1SQEkgH5MqJdJADBaQEVAFJBS4hDWgmJSQnXZMOoARKYEx2HWAlW7IxXP67eEaWzWP12VkwlODd19B3TM1KAcG7NwBLufZmMkBgBuAHwAOAPYCnxaramY0jV6ISAci1Mjsy6VWJ4N0LgE9zA3herKpvs9pBcSWCd23f+sOsjCEBYA1gZlb/mPUJadFxSBi35oblsSSkpZXILXfRsWRLBO9e655gbsRZSnizyDoOCeP25CeZw056x75rb24lNoUCkPCuzWqEzkoE7+bSGftSV+O37dmcSqR6Qi6dz7dKBO/WBWFMsZRQJ0keR88wpthL74iGtK0S1ZUEIO/ZmFUhWongXX0Eg4eVCNGQpioxNIwpop3USEgY52bndZjL+884O46LaWksTEgvK3HNMKYw80hTiRHDmKKZwk4rEQ3NiDSfN8X/6J6alsaimcImNwpjiuNfhemNwpjiGNJox+xCyhhrw++LVVU86JpmdQ8ooVBCoYRCCYUSCiUUSiiUUCihUEKhhEIJhRIKJRRKKJRQKKFQQqGEQgmFEgolFEoolFAooVBCoYRyfwkAB8ZcZ2KRdqBxAAAAAElFTkSuQmCC"/> BACK TO TOP</a>
            </div>
            <div class="container footer-content">
              <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-consultant mobile">
                  <?php 
                    if(!in_array(Request::path(), array("home", "", "/"))){ ?>
                    <a href="{{URL::to('contact')}}" class="footer-get-consultant-btn"><div>GET A CONSULTANT</div></a>
                   <?php } ?>
                 </div>
                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">

                  

                  <div class="title">
                    About Us
                  </div>
                  <div class="border-title">
                  </div>
                  <div class="description">
                    AALF was founded with idealism and determination to bring a real contribution to the development of the world of law in Indonesia. 
                  </div>
                  <div class="title">
                    Expertise
                  </div>
                  <div class="border-title">
                  </div>
                  <div class="border-links">
                    <a href="http://www.ariyantoarnaldo.com" class="link-arrow-footer" target="_blank">Law <img alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/><br/></a>
                    <a href="{{ URL::to('about') }}" class="link-arrow-footer">Legal & Tax <img alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></a>
                  </div>

                  <div class="mobile footer-consultant">
                    <div class="title <?php 
                    if(in_array(Request::path(), array("home", "", "/"))){ ?>location-title<?php } ?>">
                      Location
                    </div>
                    <div class="border-title"></div>
                    <div class="address">
                      Revenue Tower 12th Floor<br/>
                      Scbd, Jl. Jend. Sudirman No.52-53, Senayan,<br/>
                      Jakarta Selatan, Jakarta 12190<br/>
                      <a class="link-arrow-footer" href="https://www.google.com/maps/place/Revenue+Tower/@-6.2274569,106.8073847,19z/data=!4m5!3m4!1s0x2e69f1b31c95e629:0xa92869cad3d62ace!8m2!3d-6.2272093!4d106.8072548?shorturl=1">Get Direction <img alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></a><br/>
                      Monday - Friday<br/>
                      info@aalflegaltax.com<br/>
                      (62 21) 2912 7888<br/>
                      
                    </div>
                  </div>

                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margin-consultant desktop">
                <?php 
                if(!in_array(Request::path(), array("home", "", "/"))){ ?>
                <a href="{{URL::to('contact')}}" class="footer-get-consultant-btn"><div>GET A CONSULTANT</div></a>
               <?php } ?>
                <div class="title <?php 
                if(in_array(Request::path(), array("home", "", "/"))){ ?>location-title<?php } ?>">
                  Location
                </div>
                <div class="border-title"></div>
                <div class="address">
                  Revenue Tower 12th Floor<br/>
                  SCBD, Jl. Jend. Sudirman No.52-53, Senayan,<br/>
                  Jakarta Selatan, Jakarta 12190<br/>
                  <a class="link-arrow-footer" href="https://www.google.com/maps/place/Revenue+Tower/@-6.2274569,106.8073847,19z/data=!4m5!3m4!1s0x2e69f1b31c95e629:0xa92869cad3d62ace!8m2!3d-6.2272093!4d106.8072548?shorturl=1">Get Direction <img alternate-src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABsklEQVR4nO2b3W3CQBCE56IUkBKgg3QQ6CAlkA6SDkIFpgPTASVAB6QDu4PQwUarHJEFxmc75n50870gmZf1jmZ3b3UG6Y+IFCLyypQlhIjs5Rf9fc49H0nQEO1MKSJPueclalpEU75F5D333ETLDdHOVCKy8BH7w9UTMpYZgL0VdkbR0kLdpq77ZL+LAEd5bEP73WrqyOm0+6JOK63YXvoduWCE0y4pp+h3dJpftFQetd/l9NJBmcBpTaqxKzE6LRxaJndjVmIULTwLWzJ7r8QoWjys7PnOuRKjaHGhTitcKzGKFiedKzGKFjetKzGjI2jumUmEE4APY8yWTksHddqLOo6ipcEBwNIY82aMOT3mno3IqQGstSQ2w1TRlrlnZgAFAF8XetYANuqsq39IfybePd5i1zbmN2F5jIcvOx0eXBFRtPD8jfJ9I+H0GJYNgPkQwUCnBUNLoI7v9ZgAKJpfaiuWs291wfLoh5M9b83/KxjoNC9s7aDB81YIBp7T+GVNDPQUrbrHBVUykh6i8Sp4bHSI5lw9kUC0iHbsustBIqAhGj8kTAUrmt6WCte3APwAoCHbMzOiR2oAAAAASUVORK5CYII=" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></a><br/>
                  Monday - Friday<br/>
                  info@aalflegaltax.com<br/>
                  (62 21) 2912 7888<br/>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-buttom-area">
          <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="footer-buttom">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-text">
                      &copy 2022 AALFLEGAL&TAXCONSULTANTS | All Rights Reserved.
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-text-right">
                        <a href="{{ URL::to('home') }}"><img src="{{ URL::asset('assets/img/logo.svg?3')}}" alt="logo"></a>
                    </div>
                 </div>
               </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- Footer Area End Here -->
		<!-- jquery
		============================================ -->		
        <script src="{{ URL::asset('assets/js/vendor/jquery-1.11.3.min.js')}}"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
		<!-- wow JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/wow.min.js')}}"></script>
		<!-- price-slider JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery-price-slider.js')}}"></script>		
		<!-- meanmenu JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery.meanmenu.js')}}"></script>
		<!-- owl.carousel JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/owl.carousel.min.js')}}"></script>
		<!-- scrollUp JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/jquery.scrollUp.min.js')}}"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="{{ URL::asset('assets/js/plugins.js')}}"></script>
        <!-- Nivo slider js
        ============================================ -->        
        <script src="{{ URL::asset('assets/lightslider/js/lightslider.min.js')}}" type="text/javascript"></script>

        <!-- Sweet Alert -->
        <script src="{{ URL::asset('assets/js/main.js?ver=1')}}"></script>

		<!-- main JS
		============================================ -->		
        <script src="{{ URL::asset('assets/sweetalert2/sweetalert2.all.min.js')}}"></script>


        @foreach($js_files as $jf)
          <script src="{{URL::asset($jf)}}"></script>
        @endforeach

        <script>
        {!!$custom_js!!}

        <?php if(in_array(Request::path(), array("home", "", "/"))){ ?>
          loadingScreen();
        <?php } ?>
        </script>
      </div>

    </body>
</html>

