@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/partner-banner.jpg')}}) !important">
 <div class="container">
    
 	<div id="partner-content">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Always deeply understand the laws,<br/>the people and the cultures of Indonesia</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>PARTNER</h1>
			</div>
	      </div>
	      
	    </div>
	</div>
</div>

</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/partner-banner.jpg')}}) !important">
 <div class="container">
    
 	<div id="partner-content-mobile">
	    <div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <div class="banner-title expertise-right">
					<h1>Always deeply understand the laws,<br/>the people and the cultures of Indonesia</h1>
				</div>
		      </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
				<h1>PARTNER</h1>
			</div>
	      </div>
	      
	    </div>
	</div>
</div>

</div>
<!--  Banner Area End here -->

<div class="partners desktop">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 partners-container">
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-name-section">
	 		<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
	 			<div class="partner-title">PARTNERS</div>
	 			<?php $count = 0; foreach($partners as $partner){ ?>
			 		<a 
			 		fullname="<?php echo $partner->fullname; ?>"
			 		title="<?php echo $partner->title; ?>"
			 		description_1="<?php echo $partner->description_1; ?>"
			 		description_2="<?php echo $partner->description_2; ?>"
			 		photo="<?php echo URL::asset('assets/img/partners/'.$partner->photo);?>"
			 		education="<?php echo $partner->education; ?>"
			 		href="#" class="partner-name-link" <?php if($count == 0){echo 'id="initial-partner"';}?> clicked="false"><div class="partner-name"><?php echo $partner->fullname; ?></div>
			 		<div class="border-under-title-left-tiny"><img class="partner-link-border-img" alternate-src="{{URL::asset('assets/img/border-under.svg')}}" src=""></div></a>
		 		<?php $count++; } ?>
	 		</div>
	 		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
	 	</div>
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-mid-content-section partner_need_to_hide">
	 		<div class="partner-image"><img id="partner_photo" src="{{URL::asset('assets/img/partners/aryanto.svg')}}"></div>
	 		<div id="partner_education"></div>
	 	</div>
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-content-section partner_need_to_hide">
	 		<div class="partner-name-header" id="partner_fullname"></div>
	 		<div class="partner-title-header" id="partner_title"></div>
	 		<div class="border-under-title-super-tiny"><img src="{{URL::asset('assets/img/border-under-partner.svg')}}" ></div>
	 		<div class="partner-description-section" id="partner_description_1"></div>
	 		<div class="border-under-title-super-tiny"><img src="{{URL::asset('assets/img/border-under-partner.svg')}}" ></div>
	 		<div class="partner-description-section" id="partner_description_2"></div>
	 	</div>
	   
	 </div>
   </div>
 </div>
</div>


<div class="partners mobile">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 partners-container-mobile">
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-name-section">
	 		<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
	 			<div class="partner-title-mobile">Partners</div>
	 			<?php $count = 0; foreach($partners as $partner){ ?>
			 		<a 
			 		fullname="<?php echo $partner->fullname; ?>"
			 		title="<?php echo $partner->title; ?>"
			 		description_1="<?php echo $partner->description_1; ?>"
			 		description_2="<?php echo $partner->description_2; ?>"
			 		photo="<?php echo URL::asset('assets/img/partners/'.$partner->photo);?>"
			 		education="<?php echo $partner->education; ?>"
			 		href="#" class="partner-name-link" <?php if($count == 0){echo 'id="initial-partner-mobile"';}?> clicked="false"><div class="partner-name-mobile"><?php echo $partner->fullname; ?></div>
			 		</a>
		 		<?php $count++; } ?>
	 		</div>
	 	</div>
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-mid-content-section partner_need_to_hide">
	 		<div class="partner-image"><img id="partner_photo_mobile" src="{{URL::asset('assets/img/partners/aryanto.svg')}}"></div>
	 		
	 	</div>
	 	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 partners-content-section partner_need_to_hide">
	 		<ul class="lightSlider">
	 			<li>
	 				<div class="partner-name-header" id="partner_fullname_mobile"></div>
			 		<div class="partner-title-header" id="partner_title_mobile"></div>
			 		<div class="border-under-title-super-tiny"><img src="{{URL::asset('assets/img/border-under-partner.svg')}}" ></div>
			 		<div class="partner-description-section" id="partner_description_1_mobile"></div>
			 		<div class="border-under-title-super-tiny"><img src="{{URL::asset('assets/img/border-under-partner.svg')}}" ></div>
			 		<div class="partner-description-section" id="partner_description_2_mobile"></div>
	 			</li>
	 			<li>
	 				<div id="partner_education_mobile"></div>
	 			</li>
	 		</ul>
	 	</div>
	   
	 </div>
   </div>
 </div>
</div>


<!-- slider end-->
@endsection