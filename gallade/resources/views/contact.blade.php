@extends('templates.layout')
@section('title', 'Page Title')

@section('content')
<!--  Banner Area Start here -->
<div class="banner-area desktop" style="background-image:url({{URL::asset('assets/img/contact-banner.jpg')}}) !important">
 <div class="container">
    
 	<div id="contact-content">
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	          <div class="banner-title expertise-right">
	        <h1>We look forward to working together to<br/>strengthen and provide assistance</h1>
	      </div>
	        </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
	      <h1>CONTACT</h1>
	    </div>
	      </div>
	      
	    </div>
	</div>


  </div>
</div>

<div class="banner-area mobile" style="background-image:url({{URL::asset('assets/img/mobile/contact-banner.jpg')}}) !important">
 <div class="container">
    
 	<div id="contact-content-mobile">
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	          <div class="banner-title expertise-right">
	        <h1>We look forward to working together to<br/>strengthen and provide assistance</h1>
	      </div>
	        </div>
	    </div>
	    <div class="row">
	      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <div class="banner-title expertise-left">
	      <h1>CONTACT</h1>
	    </div>
	      </div>
	      
	    </div>
	</div>


  </div>
</div>
<!--  Banner Area End here -->

<div class="our-practice-area">
 <div class="container">
   <div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <div class="section-title-area-contact">
		 <h2>{{trans('messages.we_are_based')}}</h2>
	   </div>
	   <div class="border-under-title margin-after-border"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	 </div>
   </div>
</div>
</div>

<div class="contact-location">
	<div class="container">
   <div class="row">

	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 map-container-mobile">
	  <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 map-container-mobile" style="overflow: hidden;">
  	  	<iframe width="100%" height="370" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJdY80rj3xaS4R6An7XGcbgFk&key=AIzaSyC_zzRJtC4yr4lz-Qb8k3ifGP4RzSScm48" allowfullscreen></iframe>
  	  	<!--<img src="{{URL::asset('assets/img/contact_bg.png')}}" style="width:100%; height:370px;" />-->
	  </div>
	  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 address-right" style="overflow: hidden;">
	  	<h2>Location</h2>
	  	<div class="contact-page-border-under-title"><img src="{{URL::asset('assets/img/border-under.svg')}}"></div>
	  	<div class="contact-content">
	  		Revenue Tower 12th Floor<br/>
			  SCBD, Jl. Jend. Sudirman No.52-53,<br/>
			  Senayan, Jakarta Selatan, Jakarta 12190<br/>
	          <a id="getdirection-contact" href="https://www.google.com/maps/place/WeWork+Revenue+Tower/@-6.2274855,106.8071067,15z/data=!4m5!3m4!1s0x0:0x59801b675cfb09e8!8m2!3d-6.2274855!4d106.8071067">Get Direction <img id="arrow-contact" src="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG0AAAAmCAYAAADOZxX5AAABxklEQVR4nO2azW0CMRBGP6IUkBJIB5z3kk0HOWwBpIO4AqCCJRVAAZaSDuDka+gglEAHiUaaRFGA/bW9RnzvstJqpbH8NJ7xaEGa46wpnTVPQ2/ZzdEbUsUEwJuzZuOsmVR8FxRK60YO4MNZs3LW3MUOTmn9mAL4dNa8xAxKaf2RTJNaJ/LyGAEpzR9jAButd+OQgSjNP7kemfNQ9Y7SwjFTeVPfESgtLJJpKz0yvdU7SotDrvVu5aPeUVpcpnq/m/eJSmnxkSNzpleETiMxShuOcdeRGKUNT+uRGKWlQ+ORGKWlRaORGKWlSeVIjNLS5uRIbOSs+br2nbkQDgBMVpRrZtrlIJn2IBl3e+07cSFsASyyopQnKC1t9ipr/XeVIu3x2nemBaX+3BODBYBlVpSH/7FGiW5OkkgLrh1dSN614difi8HjMR12KmtbtyJKG57fVr7pStjyD8sSwH0bYWCmDYYcgc9VdasKSovLXmXV1q0qKC0OUrdes6Ls9ZvBD5QWnrU2Gkf3ra5QWji2KmvnOwKl+efk6MknlOaXs6Mnn1CaH2pHTz6htH40Hj35hNK6cdC6tRwiOKW1R0SJsKB16ywAvgErCJ6LuCRQjQAAAABJRU5ErkJggg=="/></a><br/>
	          Monday - Friday<br/>
	          (82-21) 2912 7888<br/>
	          info@aalflegaltax.com<br/>
	  	</div>
	  </div>
	  
	</div>
 </div>
</div>
</div>

<div class="contact-form-section">
	 <div class="container">
		 	<div class="row">
		 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-form-title">
		 			<h3>GOT ANY QUESTION?</h3>
		 		</div>

		 	</div>
	 </div>
 </div>

 <div class="contact-form-content">
	 <div class="container">
		 	<div class="row">
		 		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
		 		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="overflow: hidden; margin-bottom:20px;" >
		 			<form name="contact_form" id="contact_form" action="{{ URL::to('contact/submit') }}" method="POST">
			 			{{ csrf_field() }}
			 			<div class="contact-row">
			 				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				 				<div class="contact-form-field-container">
				 					<input type="text" name="name" class="form-control" placeholder="NAME" required/>
				 				</div>
				 			</div>
				 			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				 				<div class="contact-form-field-container">
				 					<input type="email" name="email" class="form-control" placeholder="E-MAIL" />
				 				</div>
				 			</div>
				 			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				 				<div class="contact-form-field-container">
				 					<input type="number" name="phone" class="form-control" placeholder="PHONE" />
				 				</div>
				 			</div>
			 			</div>
			 			<div class="contact-row">
			 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 				<div class="contact-form-field-container">
				 					<input type="text" name="subject" class="form-control" placeholder="SUBJECT" />
				 				</div>
				 			</div>
			 			</div>

			 			<div class="contact-row">
			 				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 				<div class="contact-form-field-container">
				 					<textarea name="message"></textarea>
				 				</div>
				 			</div>
			 			</div>

			 			<div class="contact-row">
			 				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
			 				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			 					<div id="submit-message-container">
				 				<input id="submit-message" class="send-message" type="submit" value="SEND MESSAGE" />
				 				</div>
				 			</div>
				 			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
			 			</div>
		 			</form>

		 		</div>
		 		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>

		 	</div>
	 </div>

 </div>

</div>


<!-- slider end-->
@endsection