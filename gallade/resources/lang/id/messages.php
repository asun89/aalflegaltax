<?php

return [
	
	//Global Content
    'welcome' => 'Selamat datang di Ariyanto Arnaldo Law Firm!!',
    'we_are_based' => 'KAMI BERLOKASI DI JAKARTA',
    'thanks_for_trust' => 'TERIMA KASIH TELAH MEMPERCAYAI KAMI',
    'vacancy_title' => 'LOWONGAN',
    'send_application_wording' => 'Kirim CV dan sertifikat anda ke',
    'requirements' => 'Kebutuhan',
    'open_position' => 'lowongan',
    'no_vacancy' => 'tidak ada lowongan',
    'home' => 'Beranda',
    'about_us' => 'Tentang',
    'expertises' => 'Keahlian',
    'partners' => 'Mitra',
    'teams' => 'Team',
    'article' => 'Artikel',
    'clients' => 'Klien',
    'events' => 'Acara',
    'career' => 'Karir',
    'contact_us' => 'Hubungi',
    'contact' => 'Hubungi',
    'consultation_message' => 'Jika anda memiliki masalah hukum ... Kami siap membantu',
    'consultation_button_text' => 'Konsultasikan',
    'top_address' => '80 San Francisco Boulevard, San Francisco U.S. ZIP 123 556',  
    'keep_in_touch' => 'Tetap Terhubung',
    'sitemap' => 'Peta Situs',
    'about_us_footer' => 'Thereare aswrta serttrmany variations of passages of Lorem Ipsum available, but the majority have suffered setyu..

                  Thereare aswrta serttrmany variations of passages of Lorem Ipsum available.',
    'working_day' => 'Sabtu - Senin',
    'article_announcement' => 'Artikel & Pemberitahuan',
    'working_address' => 'Equity Tower, 35th Floor, Suite 35C',
    'working_address_detail' => 'ARIYANTOARNALDO Law Firm
Equity Tower, 35th Floor, Suite 35C
Sudirman Central Business District (“SCBD”) Lot. 9
Jl. Jenderal Sudirman Kav. 52-53 
Jakarta 12190, Indonesia',
    'working_phone' => '(62-21) 2903 7666',
    'working_email' => 'info@ariyantoarnaldo.com',
    'working_fax' => '(62-21) 2903 7667',

    //Global Links
    'facebook_link' => 'http://facebook.com',
    'twitter_link' => 'http://twitter.com',
    'linkedin_link' => 'http://linkedin.com',
    'skype_link' => 'http://skype.com',

    //Home Content
    'home_message_1' => 'Kami adalah Firma Hukum dengan reputasi unggul yang berdedikasi untuk memberikan pelayanan hukum korporasi dan komersial yang berkualitas. Pengetahuan yang Kami dapat dari praktek dan pengalaman membuat Kami dapat menyediakan pelayanan hukum secara komprehensif kepada Klien Kami ',
    'home_message_2' => 'Pengacara bagi Kami bukan sekedar profesi tetapi adalah sebuah pilihan hidup.
Kami mensinergikan usaha dan lingkungan kerja dalam rangka mendidik dan melatih bukan semata-mata karyawan Firma Hukum tetapi seorang pengacara. 
Pengacara yang bukan  semata-mata percaya pada hukum tetapi terlebih lagi Pengacara yang percaya diri. ',
    'need_legal_help' => 'BUTUH BANTUAN HUKUM ?',
    'need_legal_help_button_text' => 'BUAT JANJI',
    'pay_nothing' => 'Tak Perlu bayar apapun 
          Kecuali kita menang',
    'our_practice_area' => 'Area Praktis Kami',
    'latest_events' => 'ACARA TERBARU',

	//About Content
    'about_title' => 'Tentang Kami',

    //Partners Content
    'view_bio' => 'Lihat Bio',
    'partners_title' => 'Mitra Kerja Kami',
    'partners_caption' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',
    'partners_detail_title' => 'Biografi Mitra Kerja',
    'partners_detail_breadcrumb' => 'Biografi',

    //Teams Content
    'teams_title' => 'AALF Legal & Tax Team',
    'teams_caption' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',

    //Career Content
    'lets_join_us' => "Mari Bergabung",
    'career_message' => 'AALF selalu bersemangat untuk bertemu orang-orang yang berdedikasi untuk karir mereka, pekerja keras, berwawasan luas dan memiliki mimpi yang panjang disebut untuk mencapai kemampuan sebagai Pengacara. Tidak hanya sebagai profesi tapi apalagi sebagai cara hidup.

Jika Anda adalah salah satu dari mereka, maka hubungi kami untuk kami selalu antusias untuk bertemu orang-orang seperti itu.

Kirimkan lamaran lengkap Anda ke alamat kantor kami atau email.

Kami selalu menghargai orang.',
    
    'quick_apply_title' => 'Kirim CV',
    'quick_apply_description' => 'Segera kirimkan CV kamu, kami akan mengevaluasi CV kamu dan akan menghubungimu dalam 1-2 minggu',
    'candidate_full_name' => 'Nama Lengkap',
    'candidate_email' => 'Email',
    'candidate_phone' => 'Nomor Telepon',
    'candidate_cv' => 'CV (Attachment)',
    'candidate_photo' => 'Foto Profil (Attachment)',
    'candidate_position' => 'Posisi yang diinginkan',
    'submit_cv' => 'SUBMIT CV',
    'candidate_facebook' => 'URL Facebook',
    'candidate_instagram' => 'URL Instagram',
    'career_alert' => 'Terima kasih telah mengirim CV kepada kami, kami akan memproses CV anda dan akan segera menghubungi anda dalam waktu 1-2 minggu',

    //Practice Area
    'practice_area_title' => 'Area pembelajaran dan praktis kami',
    'practice_area_description' => 'Kami memiliki area praktis yang telah kami tangani sampai hari ini, kami berkembang terus menerus agar dapat menjadi tim lawyer terbaik untuk anda',

    
    //Clients
    'clients_title' => 'Klien Kami',
    'clients_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapiente sint odit iusto blanditiis doloribus.',

    //Contact
    'contact_form' => 'Form Hubungi Kami',
    'contact_name' => 'Nama',
    'contact_email' => 'E-Mail',
    'contact_phone' => 'No. Telp',
    'contact_message' => 'Pesan',
    'send_message' => 'Kirim Pesan',
    'office_address' => 'Alamat Kantor',
    'contact_alert' => 'Terima kasih telah mengirim email, kami akan segera menghubungi anda.',

    //Teams
    'view_bio_teams' => 'Lihat Bio',
    'partners_title_teams' => 'Anggota Kami yang berharga',
    'partners_caption_teams' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',
];