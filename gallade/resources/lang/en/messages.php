<?php

return [

    //Global Content
    'welcome' => 'Welcome to Ariyanto Arnaldo Law Firm',
    'we_are_based' => 'WE ARE BASED ON JAKARTA',
    'thanks_for_trust' => 'THANKS FOR TRUSTING US',
    'vacancy_title' => 'VACANCY',
    'send_application_wording' => 'Send us your CV and Certificate to',
    'requirements' => 'Requirements',
    'open_position' => 'open position(s)',
    'no_vacancy' => 'no vacancy',
    'home' => 'Home',
    'about_us' => 'About',
    'expertises' => 'Expertise',
    'partners' => 'Partner',
    'teams' => 'Team',
    'article' => 'Article',
    'clients' => 'Client',
    'career' => 'Career',
    'contact_us' => 'Contact',
    'contact' => 'Contact',
    'area_of_expertise' => 'Area of Expertise',
    'consultation_message' => 'If you have any legal problem in your life ... We are available',
    'consultation_button_text' => 'Get Consultation',
    'top_address' => '80 San Francisco Boulevard, San Francisco U.S. ZIP 123 556',
    'keep_in_touch' => 'Keep in Touch',
    'sitemap' => 'Sitemap',
    'about_us_footer' => 'Thereare aswrta serttrmany variations of passages of Lorem Ipsum available, but the majority have suffered setyu..

                  Thereare aswrta serttrmany variations of passages of Lorem Ipsum available.',
    'working_day' => 'Saturday - Monday',
    'article_announcement' => 'Article & Announcement',
    'working_address' => 'Equity Tower, 35th Floor, Suite 35C',
    'working_address_detail' => 'ARIYANTOARNALDO Law Firm
Equity Tower, 35th Floor, Suite 35C
Sudirman Central Business District (“SCBD”) Lot. 9
Jl. Jenderal Sudirman Kav. 52-53 
Jakarta 12190, Indonesia',
    'working_phone' => '(62-21) 2903 7666',
    'working_email' => 'info@ariyantoarnaldo.com',
    'working_fax' => '(62-21) 2903 7667',

    //Global Links
    'facebook_link' => 'http://facebook.com',
    'twitter_link' => 'http://twitter.com',
    'linkedin_link' => 'http://linkedin.com',
    'skype_link' => 'http://skype.com',

    //Home Content
    'home_message_1' => 'We are a reputable law firm dedicated to provide quality corporate and commercial legal services. Our practical knowledge and experiences allow Us to provide comprehensive legal services to our Clients. ',
    'home_message_2' => 'Lawyering for Us is not just a profession. It is a way of life. We are combining efforts and work environment to educate and train our Employee to exceed as a Lawyer. Lawyer who not only believe in law but also in themselves.',
    'need_legal_help' => 'NEED AN APPOINTMENT FOR LEGAL HELP?',
    'need_legal_help_button_text' => 'MAKE AN APPOINTMENT',
    'pay_nothing' => 'Pay Nothing Unless 
          We Win The Case.',
    'our_practice_area' => 'Our Practice Area',
    'latest_events' => 'LATEST EVENTS',

    //About Content
    'about_title' => 'About Us',

    //Partners Content
    'view_bio' => 'View Bio',
    'partners_title' => 'Our Partners',
    'partners_caption' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',
    'partners_detail_title' => 'Partner Biography',
    'partners_detail_breadcrumb' => 'Biography',

    //Teams Content
    'teams_title' => 'AALF Legal & Tax Team',
    'teams_caption' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',

    //Career Content
    'lets_join_us' => "Let's Join Us",
    'career_message' => 'AALF always eager to meet people who are dedicated to their career, hard working, broad minded and possess a long termed dream to accomplish the ability as a Lawyer. Not only as a profession but also as a way of life.

         Should you are one of them, do not hesitate to contact Us for We are always enthusiastic to meet such people. 
Send us your Complete Resume to Our Office address or E-mail.
We always value people.',
    
    'quick_apply_title' => 'Quick Apply',
    'quick_apply_description' => 'Just send your CV, we will review your CV and will contact you back within 1-2 weeks',
    'candidate_full_name' => 'Full Name',
    'candidate_email' => 'E-Mail',
    'candidate_phone' => 'Phone Number',
    'candidate_cv' => 'CV (Attachment)',
    'candidate_photo' => 'Profile Picture (Attachment)',
    'candidate_position' => 'Position to Apply',
    'submit_cv' => 'SUBMIT CV',
    'candidate_facebook' => 'Facebook URL',
    'candidate_instagram' => 'Instagram URL',
    'career_alert' => 'Thanks for sending your CV, we will process your CV and will back to you around 1-2 week(s)',

    //Practice Area
    'practice_area_title' => 'Our Area of Study and Practice',
    'practice_area_description' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered humouror randomised words which don\'t look',

    //Clients
    'clients_title' => 'Our Clients',
    'clients_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapiente sint odit iusto blanditiis doloribus.',

    //Contact Page
    'contact_form' => 'Contact Form',
    'contact_name' => 'Name',
    'contact_email' => 'E-Mail',
    'contact_phone' => 'Phone No.',
    'contact_message' => 'Message',
    'send_message' => 'Send Message',
    'office_address' => 'Office Address',
    'contact_alert' => 'Thanks for sending an email to us, we will contact you back.',

    //Teams
    'view_bio_teams' => 'Lihat Bio',
    'partners_title_teams' => 'Anggota Tim Kami yang berharga',
    'partners_caption_teams' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati laborum ipsa, a voluptates libero possimus sapien3e sint odit iusto blanditiis doloribus.',
];