<?php  if ( ! defined("BASEPATH")) exit("No direct script access allowed");
	function generate_sidemenu()
	{
		return '<li>
		<a href="'.site_url('articles').'"><i class="fa fa-list fa-fw"></i> Articles</a>
	</li><li>
		<a href="'.site_url('cache').'"><i class="fa fa-list fa-fw"></i> Cache</a>
	</li><li>
		<a href="'.site_url('clients').'"><i class="fa fa-list fa-fw"></i> Clients</a>
	</li><li>
		<a href="'.site_url('contact_submission').'"><i class="fa fa-list fa-fw"></i> Contact submission</a>
	</li><li>
		<a href="'.site_url('expertise').'"><i class="fa fa-list fa-fw"></i> Expertise</a>
	</li><li>
		<a href="'.site_url('jobs').'"><i class="fa fa-list fa-fw"></i> Jobs</a>
	</li><li>
		<a href="'.site_url('migrations').'"><i class="fa fa-list fa-fw"></i> Migrations</a>
	</li><li>
		<a href="'.site_url('partners').'"><i class="fa fa-list fa-fw"></i> Partners</a>
	</li><li>
		<a href="'.site_url('password_resets').'"><i class="fa fa-list fa-fw"></i> Password resets</a>
	</li><li>
		<a href="'.site_url('sessions').'"><i class="fa fa-list fa-fw"></i> Sessions</a>
	</li><li>
		<a href="'.site_url('teams').'"><i class="fa fa-list fa-fw"></i> Teams</a>
	</li><li>
		<a href="'.site_url('users').'"><i class="fa fa-list fa-fw"></i> Users</a>
	</li>';
	}
