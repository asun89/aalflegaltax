<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sessions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Sessions_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('sessions/sessions_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Sessions_model->json();
    }

    public function read($id) 
    {
        $row = $this->Sessions_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user_id' => $row->user_id,
		'ip_address' => $row->ip_address,
		'user_agent' => $row->user_agent,
		'payload' => $row->payload,
		'last_activity' => $row->last_activity,
	    );
            $this->load->view('sessions/sessions_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sessions'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('sessions/create_action'),
	    'id' => set_value('id'),
	    'user_id' => set_value('user_id'),
	    'ip_address' => set_value('ip_address'),
	    'user_agent' => set_value('user_agent'),
	    'payload' => set_value('payload'),
	    'last_activity' => set_value('last_activity'),
	);
        $this->load->view('sessions/sessions_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
		'user_agent' => $this->input->post('user_agent',TRUE),
		'payload' => $this->input->post('payload',TRUE),
		'last_activity' => $this->input->post('last_activity',TRUE),
	    );

            $this->Sessions_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('sessions'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Sessions_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('sessions/update_action'),
		'id' => set_value('id', $row->id),
		'user_id' => set_value('user_id', $row->user_id),
		'ip_address' => set_value('ip_address', $row->ip_address),
		'user_agent' => set_value('user_agent', $row->user_agent),
		'payload' => set_value('payload', $row->payload),
		'last_activity' => set_value('last_activity', $row->last_activity),
	    );
            $this->load->view('sessions/sessions_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sessions'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'user_id' => $this->input->post('user_id',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
		'user_agent' => $this->input->post('user_agent',TRUE),
		'payload' => $this->input->post('payload',TRUE),
		'last_activity' => $this->input->post('last_activity',TRUE),
	    );

            $this->Sessions_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('sessions'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Sessions_model->get_by_id($id);

        if ($row) {
            $this->Sessions_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('sessions'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sessions'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');
	$this->form_validation->set_rules('user_agent', 'user agent', 'trim|required');
	$this->form_validation->set_rules('payload', 'payload', 'trim|required');
	$this->form_validation->set_rules('last_activity', 'last activity', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "sessions.xls";
        $judul = "sessions";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "User Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Ip Address");
	xlsWriteLabel($tablehead, $kolomhead++, "User Agent");
	xlsWriteLabel($tablehead, $kolomhead++, "Payload");
	xlsWriteLabel($tablehead, $kolomhead++, "Last Activity");

	foreach ($this->Sessions_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ip_address);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_agent);
	    xlsWriteLabel($tablebody, $kolombody++, $data->payload);
	    xlsWriteNumber($tablebody, $kolombody++, $data->last_activity);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Sessions.php */
/* Location: ./application/controllers/Sessions.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-09-01 13:14:03 */
/* http://harviacode.com */