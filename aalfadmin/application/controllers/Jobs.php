<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jobs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Jobs_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('jobs/jobs_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Jobs_model->json();
    }

    public function read($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'language_code' => $row->language_code,
		'job_title' => $row->job_title,
		'slot' => $row->slot,
		'requirements' => $row->requirements,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('jobs/jobs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jobs'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('jobs/create_action'),
	    'id' => set_value('id'),
	    'language_code' => set_value('language_code'),
	    'job_title' => set_value('job_title'),
	    'slot' => set_value('slot'),
	    'requirements' => set_value('requirements'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('jobs/jobs_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'language_code' => $this->input->post('language_code',TRUE),
		'job_title' => $this->input->post('job_title',TRUE),
		'slot' => $this->input->post('slot',TRUE),
		'requirements' => $this->input->post('requirements',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Jobs_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jobs'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('jobs/update_action'),
		'id' => set_value('id', $row->id),
		'language_code' => set_value('language_code', $row->language_code),
		'job_title' => set_value('job_title', $row->job_title),
		'slot' => set_value('slot', $row->slot),
		'requirements' => set_value('requirements', $row->requirements),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('jobs/jobs_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jobs'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'language_code' => $this->input->post('language_code',TRUE),
		'job_title' => $this->input->post('job_title',TRUE),
		'slot' => $this->input->post('slot',TRUE),
		'requirements' => $this->input->post('requirements',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Jobs_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jobs'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jobs_model->get_by_id($id);

        if ($row) {
            $this->Jobs_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jobs'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jobs'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('language_code', 'language code', 'trim|required');
	$this->form_validation->set_rules('job_title', 'job title', 'trim|required');
	$this->form_validation->set_rules('slot', 'slot', 'trim|required');
	$this->form_validation->set_rules('requirements', 'requirements', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "jobs.xls";
        $judul = "jobs";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Language Code");
	xlsWriteLabel($tablehead, $kolomhead++, "Job Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Slot");
	xlsWriteLabel($tablehead, $kolomhead++, "Requirements");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Jobs_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->language_code);
	    xlsWriteLabel($tablebody, $kolombody++, $data->job_title);
	    xlsWriteNumber($tablebody, $kolombody++, $data->slot);
	    xlsWriteLabel($tablebody, $kolombody++, $data->requirements);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Jobs.php */
/* Location: ./application/controllers/Jobs.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-09-01 13:14:03 */
/* http://harviacode.com */