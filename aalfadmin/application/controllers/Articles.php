<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Articles extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Articles_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('articles/articles_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Articles_model->json();
    }

    public function read($id) 
    {
        $row = $this->Articles_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'title' => $row->title,
		'content' => $row->content,
		'link' => $row->link,
		'thumbnail' => $row->thumbnail,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('articles/articles_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('articles'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('articles/create_action'),
	    'id' => set_value('id'),
	    'title' => set_value('title'),
	    'content' => set_value('content'),
	    'link' => set_value('link'),
	    'thumbnail' => set_value('thumbnail'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('articles/articles_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $link_uploaded      = false;
            $thumbnail_uploaded = false;
            $this->load->library('upload');
            $ext = pathinfo($_FILES['link']['name'], PATHINFO_EXTENSION);
            $config['upload_path'] = ASSET_PATH.'article_files/'; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
            $config['file_name'] = "file_".time().".".$ext; //nama yang terupload nantinya
            $this->upload->initialize($config);
            if($_FILES['link']['name'] && $this->upload->do_upload('link'))
            {
                $link = $this->upload->file_name;
                $link_uploaded = true;
            }

            $this->load->library('upload');
            $ext = pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
            $config2['upload_path'] = ASSET_PATH.'img/article_thumbnail/'; //Folder untuk menyimpan hasil upload
            $config2['allowed_types'] = 'jpg|jpeg|png|gif'; //type yang dapat diakses bisa anda sesuaikan
            $config2['max_size'] = '3072'; //maksimum besar file 3M
            $config2['max_width']  = '5000'; //lebar maksimum 5000 px
            $config2['max_height']  = '5000'; //tinggi maksimu 5000 px
            $config2['file_name'] = "file_".time().".".$ext; //nama yang terupload nantinya
            $this->upload->initialize($config2);
            if($_FILES['thumbnail']['name'] && $this->upload->do_upload('thumbnail'))
            {
                $thumbnail = $this->upload->file_name;
                $thumbnail_uploaded = true;
            }

            if ($link_uploaded && $thumbnail_uploaded)
            {
                $data = array(
                'title' => $this->input->post('title',TRUE),
                'content' => $this->input->post('content',TRUE),
                'link' => $link,
                'thumbnail' => $thumbnail,
                'created_datetime' => date('Y-m-d H:i:s'),
                'updated_datetime' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_by' => 1,
                );

                $this->Articles_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('articles'));

            }else{
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect(site_url('articles'));
            }
            /* Upload File */
        }
    }
    
    public function update($id) 
    {
        $row = $this->Articles_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('articles/update_action'),
		'id' => set_value('id', $row->id),
		'title' => set_value('title', $row->title),
		'content' => set_value('content', $row->content),
		'link' => set_value('link', $row->link),
        'thumbnail' => set_value('thumbnail', $row->thumbnail),
        'old_link' => set_value('link', $row->link),
		'old_thumbnail' => set_value('thumbnail', $row->thumbnail),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('articles/articles_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('articles'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            /* Upload File */
            if($_FILES['link']['name'] != "" && $_FILES['thumbnail']['name'] != "")
            {
                $this->load->library('upload');
                $ext = pathinfo($_FILES['link']['name'], PATHINFO_EXTENSION);
                $config['upload_path'] = ASSET_PATH.'article_files/'; //Folder untuk menyimpan hasil upload
                $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
                $config['file_name'] = "file_".time().".".$ext; //nama yang terupload nantinya
                $this->upload->initialize($config);

                $link_uploaded      = false;
                $thumbnail_uploaded = false;
                if($_FILES['link']['name'] && $this->upload->do_upload('link'))
                {
                    $link = $this->upload->file_name;
                    $link_uploaded = true;
                }

                $this->load->library('upload');
                $ext = pathinfo($_FILES['thumbnail']['name'], PATHINFO_EXTENSION);
                $config2['upload_path'] = ASSET_PATH.'img/article_thumbnail/'; //Folder untuk menyimpan hasil upload
                $config2['allowed_types'] = 'jpg|jpeg|png|gif'; //type yang dapat diakses bisa anda sesuaikan
                $config2['max_size'] = '3072'; //maksimum besar file 3M
                $config2['max_width']  = '5000'; //lebar maksimum 5000 px
                $config2['max_height']  = '5000'; //tinggi maksimu 5000 px
                $config2['file_name'] = "file_".time().".".$ext; //nama yang terupload nantinya
                $this->upload->initialize($config2);

                $link_uploaded      = false;
                $thumbnail_uploaded = false;
                if($_FILES['link']['name'] && $this->upload->do_upload('link'))
                {
                    $link = $this->upload->file_name;
                    $link_uploaded = true;
                }

                if($_FILES['thumbnail']['name'] && $this->upload->do_upload('thumbnail'))
                {
                    $thumbnail = $this->upload->file_name;
                    $thumbnail_uploaded = true;
                }

                if ($link_uploaded && $thumbnail_uploaded)
                {
                    $data = array(
                    'title' => $this->input->post('title',TRUE),
                    'content' => $this->input->post('content',TRUE),
                    'link' => $link,
                    'thumbnail' => $thumbnail,
                    'updated_datetime' => date('Y-m-d H:i:s'),
                    'created_by' => 1,
                    'updated_by' => 1,
                    );

                    /* Delete old File */
                    unlink(ASSET_PATH.'article_files/'.$this->input->post('old_link'));
                    unlink(ASSET_PATH.'article_files/'.$this->input->post('old_thumbnail'));
                    /* Delete old File */

                    $this->Articles_model->update($this->input->post('id', TRUE), $data);
                    $this->session->set_flashdata('message', 'Update Record Success');
                    redirect(site_url('articles'));

                }else{
                    $this->session->set_flashdata('message', $this->upload->display_errors());
                    redirect(site_url('articles'));
                }
            }
            else
            {
                /* Upload File */
                $data = array(
                'title' => $this->input->post('title',TRUE),
                'content' => $this->input->post('content',TRUE),
                'link' => $this->input->post('old_link',TRUE),
                'thumbnail' => $this->input->post('old_thumbnail',TRUE),
                'created_datetime' => date('Y-m-d H:i:s'),
                'updated_datetime' => date('Y-m-d H:i:s'),
                'created_by' => 1,
                'updated_by' => 1,
                );

                $this->Articles_model->update($this->input->post('id', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('articles'));
            }
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Articles_model->get_by_id($id);

        if ($row) {
            $this->Articles_model->delete($id);

            $row = $this->Articles_model->get_by_id($id);
            unlink(ASSET_PATH."article_files/".$row->link);
            unlink(ASSEt_PATH."img/article_thumbnail/".$row->thumbnail);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('articles'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('articles'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('content', 'content', 'trim|required');
    $this->form_validation->set_rules('link', 'link', 'trim|xss_clean');
    $this->form_validation->set_rules('thumbnail', 'thumbnail', 'trim|xss_clean');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "articles.xls";
        $judul = "articles";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Content");
	xlsWriteLabel($tablehead, $kolomhead++, "Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Thumbnail");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Articles_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->content);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->thumbnail);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Articles.php */
/* Location: ./application/controllers/Articles.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-09-01 13:14:03 */
/* http://harviacode.com */