<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Articles <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Title <?php echo form_error('title') ?></label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
        </div>
	    <div class="form-group">
            <label for="content">Content <?php echo form_error('content') ?></label>
            <textarea class="form-control" rows="3" name="content" id="content" placeholder="Content"><?php echo $content; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Link <?php echo form_error('link') ?></label>
            <input type="file" class="form-control" name="link" id="link" placeholder="Link" />

            <?php if(isset($old_link)){ ?>
            <a style="margin-top:10px;" href="<?php echo ASSET_URL; ?>article_files/<?php echo $old_link; ?>">Preview Link</a>
            <input type="hidden" name="old_link" id="old_link" value="<?php echo $old_link; ?>" />
            <?php } ?>

        </div>
	    <div class="form-group">
            <label for="varchar">Thumbnail <?php echo form_error('thumbnail') ?></label>
            <input type="file" class="form-control" name="thumbnail" id="thumbnail" placeholder="Thumbnail" />
            
            <?php if(isset($old_thumbnail)){ ?>
            <img style="margin-top:10px;" src="<?php echo ASSET_URL; ?>img/article_thumbnail/<?php echo $old_thumbnail; ?>" width="100%">
            <input type="hidden" name="old_thumbnail" id="old_thumbnail" value="<?php echo $old_thumbnail; ?>" />
            <?php } ?>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('articles') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>