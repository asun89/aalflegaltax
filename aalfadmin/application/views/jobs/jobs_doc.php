<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Jobs List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Language Code</th>
		<th>Job Title</th>
		<th>Slot</th>
		<th>Requirements</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($jobs_data as $jobs)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $jobs->language_code ?></td>
		      <td><?php echo $jobs->job_title ?></td>
		      <td><?php echo $jobs->slot ?></td>
		      <td><?php echo $jobs->requirements ?></td>
		      <td><?php echo $jobs->created_datetime ?></td>
		      <td><?php echo $jobs->updated_datetime ?></td>
		      <td><?php echo $jobs->created_by ?></td>
		      <td><?php echo $jobs->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>