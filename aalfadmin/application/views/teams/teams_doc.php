<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Teams List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Title</th>
		<th>Image</th>
		<th>Content</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($teams_data as $teams)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $teams->title ?></td>
		      <td><?php echo $teams->image ?></td>
		      <td><?php echo $teams->content ?></td>
		      <td><?php echo $teams->created_datetime ?></td>
		      <td><?php echo $teams->updated_datetime ?></td>
		      <td><?php echo $teams->created_by ?></td>
		      <td><?php echo $teams->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>