<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Cache <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="value">Value <?php echo form_error('value') ?></label>
            <textarea class="form-control" rows="3" name="value" id="value" placeholder="Value"><?php echo $value; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="int">Expiration <?php echo form_error('expiration') ?></label>
            <input type="text" class="form-control" name="expiration" id="expiration" placeholder="Expiration" value="<?php echo $expiration; ?>" />
        </div>
	    <input type="hidden" name="key" value="<?php echo $key; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('cache') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>