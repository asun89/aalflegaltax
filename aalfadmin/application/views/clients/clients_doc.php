<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Clients List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Name</th>
		<th>Description</th>
		<th>Image</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($clients_data as $clients)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $clients->name ?></td>
		      <td><?php echo $clients->description ?></td>
		      <td><?php echo $clients->image ?></td>
		      <td><?php echo $clients->created_datetime ?></td>
		      <td><?php echo $clients->updated_datetime ?></td>
		      <td><?php echo $clients->created_by ?></td>
		      <td><?php echo $clients->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>