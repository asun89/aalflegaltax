<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Sessions List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>User Id</th>
		<th>Ip Address</th>
		<th>User Agent</th>
		<th>Payload</th>
		<th>Last Activity</th>
		
            </tr><?php
            foreach ($sessions_data as $sessions)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $sessions->user_id ?></td>
		      <td><?php echo $sessions->ip_address ?></td>
		      <td><?php echo $sessions->user_agent ?></td>
		      <td><?php echo $sessions->payload ?></td>
		      <td><?php echo $sessions->last_activity ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>