<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Sessions Read</h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <table class="table">
	    <tr><td>User Id</td><td><?php echo $user_id; ?></td></tr>
	    <tr><td>Ip Address</td><td><?php echo $ip_address; ?></td></tr>
	    <tr><td>User Agent</td><td><?php echo $user_agent; ?></td></tr>
	    <tr><td>Payload</td><td><?php echo $payload; ?></td></tr>
	    <tr><td>Last Activity</td><td><?php echo $last_activity; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('sessions') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table><?php $this->load->view('templates/footer');?>